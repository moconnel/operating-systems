/* Author: Michael O'Connell
   Title: Master Control Program (MCP) Ghost in the Shell Part 2
   Class: CIS 415 Operating Systems at University of Oregon, Fall 2020
   Date: 11/10/2020

   Description:
   MCP Ghost in the Shell Part 2 is an addition to Part 1 and implements a way for 
   the MCP to stop all forked (MCP) child processes right before they call execvp(). 
   Each forked child process waits for a SIGUSR1 signal before calling execvp(). Once a child process
   recieves the SIGUSR1 signal, it launches the associated workload program with the
   execvp() call.

   It also implements a mechanism for the MCP to signal a running process to stop and 
   then continue again (sending SIGSTOP and SIGCONT, respectively).

   Implementation:
   1. Immediately after each program is created using the fork() system call, the forked
      MCP child process waits until it recieves a SIGUSR1 signal before calling execvp(). 
   
   2. After all the MCP child processes have been forked and are now waiting, the MCP parent 
      process must send each child process a SIGUSR1 signal (at the same time) to wake them up.
    
   3. Each child process will return from the sigwait() and call execvp() to run the workload program.
    
   4. After all of the workload programs have been launched and are now executing, MCP must 
      send each child process a SIGSTOP signal to suspend them. 

   5. After all of the child processes have been suspended, the MCP must send each child process a SIGCONT
      signal to wake them back up.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>

#define MAX_CHARS 126

void script_print (pid_t* pid_ary, int size);
void deallocate_memory(char** command_string, pid_t** pid_arr, char*** command_arr, int allocated_num_args, FILE** fp_in);
void signaler(pid_t* pool, int size, int signal);

int main(int argc, char* argv[])
{
    /* Verify and open input file */
    FILE* fp_in;
    if(argc != 2)
    {
        fprintf(stderr, "Incorrect program input. Correct usage: ./part1 input_file.txt\n");
        exit(EXIT_FAILURE);
    }
    fp_in = fopen(argv[1], "r");
    if(!fp_in)
    {
        fprintf(stderr, "Error opening input file: %s\n", argv[1]);
        exit(EXIT_FAILURE); 
    }
    
    /* condition for loop to launch processes */
    int done = 0; 
    
    /* getline() vars */
    char* command_string = NULL;
    size_t bufsize = 0;

    /* strtok_r() vars */
    char* strtok_r_ptr;
    char* strtok_r_token;
    
    /* pid_arr and command_arr vars
       
       Note that we initially allocate for an arbitrary
       number of processes and arguments and reallocate
       as needed
    */
    int allocated_num_process = 12;
    int allocated_num_args = 5;
    int num_process = 0; 
    int num_args = 0; 

    /* Allocate pid_arr */
    pid_t* pid_arr = (pid_t*) malloc(sizeof(pid_t) * allocated_num_process);
    
    /* Initalize pid_arr */
    memset(pid_arr, 0, allocated_num_process);

    /* Initalize the signal set - set of signals you want each child
       process to wait (via sigwait()) until they recieve 
    */
    sigset_t signals;
    sigemptyset(&signals);
    sigaddset(&signals, SIGUSR1);
    sigaddset(&signals, SIGSTOP);
    sigaddset(&signals, SIGCONT);
    sigaddset(&signals, SIGINT);
    int sig_num; // signal number of matched signal in signals for child process

    /* Allocate command_arr */
    char** command_arr = (char**) malloc(sizeof(char*) * allocated_num_args);
    for(int i = 0; i < allocated_num_args; i++)
    {
        command_arr[i] = (char*) malloc(sizeof(char) * MAX_CHARS);
    }
    while(!done)
    {
        /* Part1 Step 1. Read in command and its arguments, line by line, from the input file. 
              
              This is done by getting each line from the input file and modifying it and putting 
              its contents into a 2D-char array (string array) such that execvp() can take the array 
              as input to launch the specified command with its arguments.
        */
        getline(&command_string, &bufsize, fp_in);
        // remove newline character from command (if it's in the line)
        if(command_string[strlen(command_string) - 1] == '\n')
        {
            command_string[strlen(command_string) - 1] = '\0';
        }
        // if end of file, set condition to exit loop
        if(feof(fp_in))
        {
            done = 1;
            continue;
        }
        // If there are more processes than we initially allocated for, reallocate for double
        if(num_process > allocated_num_process)
        {
            int old_allocated_num_process = allocated_num_process;
            allocated_num_process = allocated_num_process * 2;
            pid_arr = (pid_t*) realloc(pid_arr, (sizeof(pid_t) * allocated_num_process));
            // pid_arr wasn't reallocated correctly, exit program
            if(pid_arr == NULL)
            {
                fprintf(stderr, "realloc() for pid_arr failed.\n");
                deallocate_memory(&command_string, &pid_arr, &command_arr, allocated_num_args, &fp_in);
                exit(EXIT_FAILURE);
            }
            // initalize the new memory block addition
            for(int i = old_allocated_num_process; i < allocated_num_process; i++)
            {
                pid_arr[i] = 0;
            }
        }
        /* initialize command_arr */
        for(int i = 0; i < allocated_num_args; i++)
        {
            memset(command_arr[i], '\0', MAX_CHARS);
        }
        /* Tokenize command string */
        strtok_r_token = strtok_r(command_string, " ", &strtok_r_ptr);
        num_args = 0;
        while(strtok_r_token != NULL)
        {
            /* if more args than we initially allocated for, reallocate for double
               
               num_args >= allocated_num_args - 1 guarentees room for 1 additional array space to NULL
               terminate the char* array (e.g. {"./iobound", "seconds", "5", NULL})

               Note that as long as the element following the last element necessary for a call to execvp()
               is NULL, the other arguments following NULL are irrelevant. For example, {"./iobound", "-seconds", "5", NULL, "test"}
               will still run ./iobound -seconds 5 via execvp(). 
               This is relevant because we may end up with an array larger than all the elements we need to call
               execvp() + 1. In this case (as explained above), the call to execvp will still work as long as it's NULL-terminated.
            */
            if(num_args >= allocated_num_args - 1)
            {
                int old_allocated_num_args = allocated_num_args;
                char** old_command_arr = command_arr;
                allocated_num_args = allocated_num_args * 2;
                command_arr = (char**) realloc(command_arr, (sizeof(char*) * allocated_num_args));
                if(command_arr == NULL)
                {
                    fprintf(stderr, "reallocation for command_arr failed.\n");
                    deallocate_memory(&command_string, &pid_arr, &old_command_arr, old_allocated_num_args, &fp_in);
                    exit(EXIT_FAILURE);
                }
                /* allocate and initalize new addition to memory block from realloc() */
                for(int i = old_allocated_num_args; i < allocated_num_args; i++)
                {
                    command_arr[i] = (char*) malloc(sizeof(char) * MAX_CHARS);
                    memset(command_arr[i], '\0', MAX_CHARS);
                }
            }
            strcpy(command_arr[num_args], strtok_r_token);
            strtok_r_token = strtok_r(NULL, " ", &strtok_r_ptr);
            num_args++;
        }
        // NULL-terminating command_arr for execvp() (see above for an explanation)
        char* tmp = command_arr[num_args];
        command_arr[num_args] = NULL;

        /* Part 1 Step 2. Create child process to run the specified command for the current line of the input file*/
        pid_arr[num_process] = fork();
        if(pid_arr[num_process] < 0)
        {
            fprintf(stderr, "Creating child process failed.\n");
            command_arr[num_args] = tmp;
            deallocate_memory(&command_string, &pid_arr, &command_arr, allocated_num_args, &fp_in);
            exit(EXIT_FAILURE);
        }
        // child process
        if(pid_arr[num_process] == 0)
        {
            fprintf(stdout, "DEBUGGING: Parent process: %d has created child process: %d\n", getppid(), getpid());
            
            // Before the child process is waiting let user know
            printf("Child Process: %d - Waiting for SIGUSR1...\n", getpid());
            
            /* Change process' blocked signals - allow OS to "hang on" to signal temporarily, 
               rather than have it be delivered immediately 
            */
            sigprocmask(SIG_BLOCK, &signals, NULL);

            /* 1. Each child process will wait until it recieves the SIGUSR1 signal */
            sigwait(&signals, &sig_num);

            /* 3. Child recieves SIGUSR1 signal */  
            // After the child recieves the signal let user know
            printf("Child Process: %d - Recieved signal: SIGUSR1 - Calling exec().\n", getpid());
            
            // child process' inherited process address space (via fork()) is 
            // ovewritten with new program (the executable, iobound) via execvp
            execvp(command_arr[0], command_arr);
            
            // if we made it here, starting program failed
            fprintf(stderr, "Starting program: %s (via execvp) failed.\n", command_arr[0]);
            command_arr[num_args] = tmp;
            deallocate_memory(&command_string, &pid_arr, &command_arr, allocated_num_args, &fp_in);
            exit(EXIT_FAILURE);
        }

        command_arr[num_args] = tmp;

        num_process++;
        
    }

    sleep(4);

    /* Send signals to the child processes and let processes run in between 
        via sleep 
    */
    /* 2. MCP parent process must send each child process a SIGUSR1 signal (at the same time)
          to wake them up. 
    */
    signaler(pid_arr, num_process, SIGUSR1);
    sleep(3);
    /* 4. MCP must send each child process a SIGSTOP signal to suspend them. */
    signaler(pid_arr, num_process, SIGSTOP);
    sleep(5);
    /* 5. After all of the child processes have been suspended, the MCP must send each child process a SIGCONT
          signal to wake them back up.
    */
    signaler(pid_arr, num_process, SIGCONT);

    /* Part1 Step 3. 
       
       6. Wait for all the children processes to finish 

       Note that because the child processes are running
       a different program, we only have the parent process
       by this point of execution in our code. Therefore, we
       can simply call waitpid() once for each parent process id
       without worrying about if we're calling it on a child process.
       In other words, the array at this point in execution is the
       just from the parent process.   
    */ 
    for(int i = 0; i < num_process; i++)
    {
        waitpid(pid_arr[i], NULL, 0);
    }
    
    // For debugging
    script_print(pid_arr, num_process); 

    /* Deallocate memory */
    deallocate_memory(&command_string, &pid_arr, &command_arr, allocated_num_args, &fp_in);
    
    /* Part1 Step 4. After all processes have terminated and memory has been deallocated, MCP must exit */
    exit(EXIT_SUCCESS);
}

/* Helper function given for debugging. Creates a script ("top_script.sh")
   that is then used to call "top" for all the parent processes via the shell command:
   "./lab4 3 | gnome-terminal -- bash top_script.sh"

   Alternatively, another way to debug is to execute the program normally,
   call top from another teminal window and observe the amount of processes running.
   
   Note that "iobound" and "cpubound" are cpu-heavy process only created for
   observing processes running in real time (via some form of "top").
*/

void script_print (pid_t* pid_ary, int size)
{
    FILE* fout;
    fout = fopen ("top_script.sh", "w");
    fprintf(fout, "#!/bin/bash\ntop");
    for (int i = 0; i < size; i++)
    {
        fprintf(fout, " -p %d", (int)(pid_ary[i]));
    }
    fprintf(fout, "\n");
    fclose (fout);
}

/* Helper method to deallocate memory from all variables/arrays that have been dynamically allocated throughout the program's
   lifecycle. Using a method to do this is beneficial due to all the conditional program blocks that result in exiting (via exit())
   the program.

   Every variable/array is taken in by reference and deallocated appropriately.
*/
void deallocate_memory(char** command_string, pid_t** pid_arr, char*** command_arr, int allocated_num_args, FILE** fp_in)
{
    /* Deallocate memory */
    free((*command_string));
    free((*pid_arr));
    for(int i = 0; i < allocated_num_args; i++)
    {
        free((*command_arr)[i]);
    }
    free((*command_arr));

    /* Close input file */
    fclose((*fp_in));
}

/* Helper method to send a signal to all child processes
   in pid_t pool (pid_arr in main())
*/
void signaler(pid_t* pool, int size, int signal)
{
    sleep(2);

    // loop through each child process
    for(int i = 0; i < size; i++)
    {
        printf("Parent process: %d - Sending signal: %d to child process: %d\n", getpid(), signal, pool[i]);
        // send the signal to each child
        kill(pool[i], signal);
    }
}