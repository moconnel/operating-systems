/*
* Description: Project 1 .c file. Contains method definitions for the commands listed in the project
*			   Description. Additional information for each method is detailed above it.
*
* Author: Michael O'Connell
*
* Date: 10/18/2020
*/
#include <stdio.h> // for rename (sys call)
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <dirent.h>
#include "command.h"

/* file directives */
#define MAX_CHAR 80
#define MAX_FILES 70
#define PROGRAM_FILE_NUM 6
char* program_file_names[] = {"command.c", "command.h", "main.c", "command.o", "main.o", "pseudoshell"};

/* Mimics Unix "ls" command:
 * Shows the names of files and folders in current directory
 *
 * Implementation steps:
 * (1) get pathname of current directory
 * (2) open current directory stream (using pathname)
 * (3) read each directory entry
 * (4) output contents of directory entry using write, with 1 = stdout
 *
 * Note:
 * - There is minimal error checking, to improve check for NULL pointer
 *   after each system call (read Linux man pages for specific errors).
 *
 * - Due to readdir() behavior, order of files/directories is undefined
     (specifically when each dir_entry is accessed).
 *   If alphabetical order is desired, use a sorting algorithm to implement.
*/
void listDir()
{
    /* Directory handling variables */
    size_t size = 128;
    char* cwd_buf = (char*) malloc(sizeof(char) * size);
    DIR* cwd_ptr; // (current working) directory stream 
    struct dirent* directory_entry; // dirent structure representing directory entry in directory stream
    
    /* File handling variables */
    int fp_directory_entry; // for opening file in current directory entry
    char* file_buf = (char*) malloc(sizeof(char) * size);

    /* Get current working directory */
    getcwd(cwd_buf, size);
    if(cwd_buf == NULL)
    {
        write(2, "Error getting current directory.\n", strlen("Error getting current directory.\n"));
        free(cwd_buf);
        exit(EXIT_FAILURE);
    }

    /* Open current working directory */
    cwd_ptr = opendir(cwd_buf);
    if(cwd_ptr == NULL)
    {
        write(2, "Error opening current directory.\n", strlen("Error opening current directory.\n"));
    }

    /* Read directory stream (all files in directory) */
    directory_entry = readdir(cwd_ptr);
    while(directory_entry != NULL)
    {
        /* Open file for reading */
        fp_directory_entry = open(directory_entry->d_name, O_RDONLY);
        if(fp_directory_entry == -1)
        {
            write(2, "Error opening file in directory stream.\n", strlen("Error opening file in directory stream.\n"));
        }

        /* Skip program files */
        unsigned int skip = 0;
        for(int i = 0; i < PROGRAM_FILE_NUM; i++)
        {
            if(strcmp(program_file_names[i], directory_entry->d_name) == 0)
            {
                skip = 1;
            }
        }
        if(skip == 1)
        {
            directory_entry = readdir(cwd_ptr);
            continue;
        }

        /* Write file/directory names with a newline (from directory stream) to stdout (-1 as a return for write indicates error) */
        if(write(1, strcat(directory_entry->d_name, "\n"), strlen(directory_entry->d_name) + 1) == -1)
        {
            write(2, "Error writing to standard out.\n", size);
        }

        directory_entry = readdir(cwd_ptr);
    }

    // finish with a newline
    write(1, "\n", 1);

    /* Deallocate memory */
    free(cwd_buf);
    free(file_buf);

    /* Close directory */
    closedir(cwd_ptr);
} 

/* Mimics Unix pwd command
 * Outputs current directory
 *
 * Implementation steps:
 * (1) get pathname of current directory.
 * (2) output pathname (using write)
 *
 */
void showCurrentDir()
{
     /* Directory handling variables */
    size_t size = 128;
    char* cwd_buf = (char*) malloc(sizeof(char) * size);

    /* Get current working directory */
    getcwd(cwd_buf, size);
    if(cwd_buf == NULL)
    {
        write(2, "Error getting current directory.\n", strlen("Error getting current directory.\n"));
        free(cwd_buf);
        exit(EXIT_FAILURE);
    }
    write(1, strcat(cwd_buf, "\n"), strlen(cwd_buf) + 1);

    free(cwd_buf);
}

/* Mimics Unix mkdir command
 * Creates new directory with the filename specified by dirName
 *
 * Implementation steps:
 * (1) Use mkdir (system call) to create new directory
 *
 * Mode field indicates:
 * S_IRWXU owner has read, write, and execute permission
 * S_IRWXG group has read, write, and execute permission
 * S_IROTH others have read permission
 * S_IXOTH others have execute permission
 */
void makeDir(char *dirName)
{
    int success;
    /* create new directory */
    success = mkdir(dirName, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if(success == -1)
    {
        write(2, "mkdir: Error creating directory.\n", strlen("mkdir: Error creating directory.\n"));
    }
}

/* Mimics Unix cd command
 *
 * Changes current working directory (for current process)
 *
 * Implementation steps:
 * (1) use chdir (system call) to change current working directory
 */
void changeDir(char *dirName)
{
    int success;

    success = chdir(dirName);

    if(success == -1)
    {
        write(2, "Error: cd failed (probably not a valid directory).\n", strlen("Error: cd failed (probably not a valid directory).\n"));
    }

}

/* Mimics the Unix cp command
 * Copies source file path to destination file path.
 *
 * Implementation steps:
 * (1) Validate src file exists
 * (2) get src file permissions
 * (3) create destination file (or open existing specified destination file)
 * (4) transfer src file data to dst file
 */
void copyFile(char *sourcePath, char *destinationPath)
{
    int src, dst;
    int file_stat_success;
    char* src_err_msg = "Error: cp failed (not a valid source file).\n";
    char* src_err_msg_perm = "Error: cp failed (failed to get source file permissions).\n";
    char* same_file_err_msg = "Error: cp failed (src and dst are the same file).\n";
    char* dst_err_msg_perm = "Error: cp failed (failed to get destination file permissions).\n";
    char* dst_err_msg = "Error: cp failed (failed to create or open destination file).\n";
    char* program_file_err_msg = "Error: cp failed (attempt to access program files).\n";

    /* Prevent program files from being targeted - check source and destination st_ino against the program file's st_ino */
    int success_file_stat_prevent, success_file_stat_d_prevent;
    struct stat file_stat_prevent;
    success_file_stat_prevent = stat(sourcePath, &file_stat_prevent);
    struct stat file_stat_d_prevent;
    success_file_stat_d_prevent = stat(destinationPath, &file_stat_d_prevent);
    struct stat program_file_stat;
    int program_file_stat_success;
    if(success_file_stat_prevent == 0 && success_file_stat_d_prevent == 0)
    {
        for(int i = 0; i < PROGRAM_FILE_NUM; i++)
        {
            program_file_stat_success = stat(program_file_names[i], &program_file_stat);
            if((program_file_stat_success == 0) && (file_stat_prevent.st_ino == program_file_stat.st_ino || file_stat_d_prevent.st_ino == program_file_stat.st_ino))
            {
                write(2, program_file_err_msg, strlen(program_file_err_msg));
                write(2, "\n", 1);
                return;
            }
        }
    }

    
    /* Validate src file exists */
    if((src = open(sourcePath, O_RDONLY)) == -1)
    {
        write(2, src_err_msg, strlen(src_err_msg));
        write(2, "\n", 1);
        return;
    }

    // get permissions for source file
    struct stat file_stat;
    struct stat file_stat_d;
    file_stat_success = stat(sourcePath, &file_stat);
    if(file_stat_success < 0)
    {
        write(2, src_err_msg_perm, strlen(src_err_msg_perm));
        write(2, "\n", 1);
        close(src);
        return;
    }

    // re-configure path for directories - statement tests whether source is valid and
    // whether source is directory
    char src_name[MAX_CHAR];
    file_stat_success = stat(destinationPath, &file_stat_d);
    if(file_stat_success == 0 && S_ISDIR(file_stat_d.st_mode))
    {
        // change path name appropriately
        // e.g. <<< cp test.txt ..
        // will replace .. with ../test.txt
        // so rename will work correctly
        // implmentation is to find what index '/' exists in sourcePath
        // and copy over everything after it to a new char array
        //
        // note: a more elegant solution is to use basename() in libgen.h
        // but outside libraries aren't allowed for this project

        // (1) get filename from sourcePath
        unsigned int sourcePath_len = strlen(sourcePath);
        int filename_index = -1;
        unsigned int src_name_index;
        unsigned int i;
        for(i = sourcePath_len - 1; i > 0; i--)
        {
            // first instance of '/' (linux), filename is after
            if(sourcePath[i] == '/')
            {
                filename_index = i + 1;
                break;
            }
        }

        // (2) copy filename over, with null terminated byte
        // if filename_index was never updated, there's no "/"
        // in sourcePath
        if(filename_index != -1)
        {
            src_name_index = 0;
            for(i = filename_index; i < sourcePath_len + 1; i++)
            {
                src_name[src_name_index] = sourcePath[i];
                src_name_index++;
            }
        }
        // file_name_index was never updated, stick with sourcePath
        // avoids two seperate strcat sequences (see below)
        else
        {
            strcpy(src_name, sourcePath);
        }

        strcat(destinationPath, "/");
        strcat(destinationPath, src_name);

        // get info based on destination path (necessary to determine if src and dst are the same file based on st_ino)
        file_stat_success = stat(destinationPath, &file_stat_d);
    }

    // checking whether src and dst are the same file by comparing st_ino (unique) identifiers for each file
    // this is necessary for cases such as cp file.txt ./file.txt, cp file.txt file.txt, cp ./file.txt file.txt, etc.
    if(file_stat_success == 0 && file_stat.st_ino == file_stat_d.st_ino)
    {
        write(2, same_file_err_msg, strlen(same_file_err_msg));
        write(2, "\n", 1);
        close(src);
        return; 
    }

    /* Create dst file (or overwrite if it exists) */
    // notes:
    // - creat is open() with O_CREAT|O_WRONLY|O_TRUNC flags
    // - using the same permissions as source
    if((dst = creat(destinationPath, file_stat.st_mode)) == -1)
    {
        write(2, dst_err_msg, strlen(dst_err_msg));
        write(2, "\n", 1);
        close(src);
        return; 
    }

    /* Transfer src data to dst */
    // use bytes_copied for debugging if useful
    off_t bytes_copied = 0;
    sendfile(dst, src, &bytes_copied, file_stat.st_size);

    close(src);
    close(dst);
}

/* Mimics the Unix mv command
 * Functionality is limited to moving files.
 * Moves file from the location specified by sourcePath to the location
 * specified by destinationPath.
 *
 * Implementation steps:
 * (1) Determine if destinationPath is a non-explicit directory (e.g. mv test.txt ..)
 *     If so, re-configure path.
 * (2) Use rename linux system call to move sourcePath to destinationPath
 */
void moveFile(char *sourcePath, char *destinationPath)
{
    char* err_msg = "Error: mv failed.\n";
    char* program_file_err_msg = "Error: mv failed (attempt to access program files).\n";
    int success;
    struct stat file_stat;
    int success_file_stat;
    struct stat file_stat_d;
    int success_file_stat_d;

    /* Prevent program files from being targeted - check source and destination st_ino against the program file's st_ino */
    int success_file_stat_prevent, success_file_stat_d_prevent;
    struct stat file_stat_prevent;
    success_file_stat_prevent = stat(sourcePath, &file_stat_prevent);
    struct stat file_stat_d_prevent;
    success_file_stat_d_prevent = stat(destinationPath, &file_stat_d_prevent);
    struct stat program_file_stat;
    int program_file_stat_success;
    if(success_file_stat_prevent == 0 && success_file_stat_d_prevent == 0)
    {
        for(int i = 0; i < PROGRAM_FILE_NUM; i++)
        {
            program_file_stat_success = stat(program_file_names[i], &program_file_stat);
            if((program_file_stat_success == 0) && (file_stat_prevent.st_ino == program_file_stat.st_ino || file_stat_d_prevent.st_ino == program_file_stat.st_ino))
            {
                write(2, program_file_err_msg, strlen(program_file_err_msg));
                write(2, "\n", 1);
                return;
            }
        }
    }

    // re-configure path for directories - statement tests whther source is valid and
    // whether source is directory
    char src_name[MAX_CHAR];
    if(stat(destinationPath, &file_stat_d) == 0 && S_ISDIR(file_stat_d.st_mode))
    {
        // change path name appropriately
        // e.g. <<< cp test.txt ..
        // will replace .. with ../test.txt
        // so rename will work correctly
        // implmentation is to find what index '/' exists in sourcePath
        // and copy over everything after it to a new char array
        //
        // note:
        // a more elegant solution is to use basename() in libgen.h
        // but outside libraries aren't allowed for this project

        // (1) get filename from sourcePath
        unsigned int sourcePath_len = strlen(sourcePath);
        int filename_index = -1;
        unsigned int src_name_index;
        unsigned int i;
        for(i = sourcePath_len - 1; i > 0; i--)
        {
            // first instance of '/' (linux), filename is after
            if(sourcePath[i] == '/')
            {
                filename_index = i + 1;
                break;
            }
        }

        // (2) copy filename over, with null terminated byte
        // if filename_index was never updated, there's no "/"
        // in sourcePath
        if(filename_index != -1)
        {
            src_name_index = 0;
            for(i = filename_index; i < sourcePath_len + 1; i++)
            {
                src_name[src_name_index] = sourcePath[i];
                src_name_index++;
            }
        }
        // file_name_index was never updated, stick with sourcePath
        // avoids two seperate strcat sequences (see below)
        else
        {
            strcpy(src_name, sourcePath);
        }

        strcat(destinationPath, "/");
        strcat(destinationPath, src_name);
    }

    // check if src and dst are the same file
    success_file_stat = stat(sourcePath, &file_stat);
    success_file_stat_d = stat(destinationPath, &file_stat_d);
    if(success_file_stat == 0 && success_file_stat_d == 0 && file_stat.st_ino == file_stat_d.st_ino)
    {        
        write(2, err_msg, strlen(err_msg));
        write(2, "\n", 1);
        return;
    }

    success = rename(sourcePath, destinationPath);

    if(success < 0)
    {
        write(2, err_msg, strlen(err_msg));
        write(2, "\n", 1);
        return;
    }
}

/* Unix the rm command
 * Only for removing files (not directories).
 * Removes specified file from the current directory
 *
 * Implementation steps:
 * (1) Implemented using the unlink() system call
 */
void deleteFile(char *filename)
{
    char* err_msg = "Error: rm failed (most likely wrong filename).\n";
    char* program_file_err_msg = "Error: rm failed (attempt to access program file).\n";

    int success;

    /* Prevent program files from being targeted - check source st_ino against the program file's st_ino */
    int success_file_stat_prevent;
    struct stat file_stat_prevent;
    success_file_stat_prevent = stat(filename, &file_stat_prevent);
    struct stat program_file_stat;
    int program_file_stat_success;
    if(success_file_stat_prevent == 0)
    {
        for(int i = 0; i < PROGRAM_FILE_NUM; i++)
        {
            program_file_stat_success = stat(program_file_names[i], &program_file_stat);
            if((program_file_stat_success == 0) && (file_stat_prevent.st_ino == program_file_stat.st_ino))
            {
                write(2, program_file_err_msg, strlen(program_file_err_msg));
                write(2, "\n", 1);
                return;
            }
        }
    }

    success = unlink(filename);

    if(success < 0)
    {
        write(2, err_msg, strlen(err_msg));
        write(2, "\n", 1);
        return;
    }
}

/* Mimics Unix cat command
*  Displays file's output to stdout
*
*  Implementation steps:
*  (1) open file
*  (2) read file contents to buffer
*  (3) write buffer to stdout
*/
void displayFile(char *filename)
{
    int file_dst;
    char* err_open_msg = "Error: cat failed (failed to open file).\n";
    char* err_data_msg = "Error: cat failed (failed to get file data).\n";
    char* program_file_err_msg = "Error: cat failed (attempt to access program file).\n";

    /* Prevent program files from being targeted - check source st_ino against the program file's st_ino */
    int success_file_stat_prevent;
    struct stat file_stat_prevent;
    success_file_stat_prevent = stat(filename, &file_stat_prevent);
    struct stat program_file_stat;
    int program_file_stat_success;
    if(success_file_stat_prevent == 0)
    {
        for(int i = 0; i < PROGRAM_FILE_NUM; i++)
        {
            program_file_stat_success = stat(program_file_names[i], &program_file_stat);
            if((program_file_stat_success == 0) && (file_stat_prevent.st_ino == program_file_stat.st_ino))
            {
                write(2, program_file_err_msg, strlen(program_file_err_msg));
                write(2, "\n", 1);
                return;
            }
        }
    }
    
    /* Open file */
    file_dst = open(filename, O_RDONLY);

    if(file_dst == -1)
    {
        write(2, err_open_msg, strlen(err_open_msg));
        write(2, "\n", 1);
        return;
    }

    // get size for file
    struct stat file_stat;
    if(stat(filename, &file_stat) < 0)
    {
        write(2, err_data_msg, strlen(err_data_msg));
        write(2, "\n", 1);
        close(file_dst);
        return;
    }

    /* read contents from a file to buffer */
    char buf[file_stat.st_size];
    read(file_dst, buf, file_stat.st_size);

    /* read contents of file (buffer) to stdout */
    write(1, buf, file_stat.st_size);

    // end with a newline
    write(1, "\n", 1);

    close(file_dst);
}
