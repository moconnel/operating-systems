#!/bin/bash
set -x #echo on
echo -e "*** USER INPUT ***\n"
make
echo -e "*** TESTING CD ***\n"
./expect #run program (in valgrind), mimic user input, you may need to install expect
echo -e "*** FILE INPUT ***\n"
valgrind ./pseudoshell -f user_input.txt
cat output.txt