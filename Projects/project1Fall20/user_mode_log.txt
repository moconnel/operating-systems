==9423== Memcheck, a memory error detector
==9423== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==9423== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==9423== Command: ./pseudoshell
==9423== 
>>> pwd 
/home/michael/Documents/operating-systems/Projects/project1Fall20
>>> ls
test
correct_user_output_test.txt
Screenshots
tools
monkey.txt
.
Makefile
output.txt
file_mode_log.txt
Project 1 - Description.pdf
..
user_mode_log.txt
input.txt

>>> rm monkey.txt
>>> pwd
/home/michael/Documents/operating-systems/Projects/project1Fall20
>>> ls
test
correct_user_output_test.txt
Screenshots
tools
.
Makefile
output.txt
file_mode_log.txt
Project 1 - Description.pdf
..
user_mode_log.txt
input.txt

>>> cd test
>>> exit

==9423== 
==9423== HEAP SUMMARY:
==9423==     in use at exit: 0 bytes in 0 blocks
==9423==   total heap usage: 25 allocs, 25 frees, 69,640 bytes allocated
==9423== 
==9423== All heap blocks were freed -- no leaks are possible
==9423== 
==9423== For counts of detected and suppressed errors, rerun with: -v
==9423== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
