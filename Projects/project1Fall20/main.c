/*
* Description: 
* Pseudo Shell's main.c. Collects user/file input, parses the input,
* and calls appropriate commands or raises appropriate errors.
*			   
* Author: Michael O'Connell
*
* Date: 10/18/20
*
* main.c generally works by:
* 1. Determining what mode the program will run in (file or user)
* 2. Allocate memory as appropriate
* 3. Recieving user/file command
* 4. Tokenizing (breaking up into smaller parts) the command string
* 5. Determining what command has been called and if appropriate arguments were given
* 6. If command and arguments are correct, call the specified command.
* 7. Determine if there are more commands in the command string.
	 If a control code was used to break up a sequence of commands,
	 check that it's used correctly. If so, steps 4-6. If not, raise
	 an error.
* 8. Close files/deallocate memory as appropriate
*/
/*-------------------------Preprocessor Directives---------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include "command.h"

// number of shell commands defined
#define NUM_COMMANDS 8

/*---------------------------------------------------------------------------*/

/*-------------------------Function/Struct declarations----------------------*/
struct command_struct
{
	char* name;
	char** args;
	int num_args;
};

void allocate_memory(struct command_struct (*all_defined_commands)[], int command_struct_argument_size);
void deallocate_memory(struct command_struct (*all_defined_commands)[], int command_struct_argument_size, char** command_string);
void call_command(struct command_struct command);

/*---------------------------------------------------------------------------*/

/*-----------------------------Program Main----------------------------------*/
int main(int argc, char** argv) 
{
	/* string handling variables */
	size_t bufsize = 0;
	char* command_string = NULL;
	char* strtok_r_ptr;
	char* strtok_r_token;
	char* current_command;

	/* program/system variables */
	bool exit_program = false;
	bool exit_input_parsing; // exits loop that parses user input, errors and end of input string will cause an exit DOES NOT exit program
	bool file_mode = false;

	/* Command structs - necessary data for each command */
	struct command_struct listDir = {"ls", NULL, 0};
	struct command_struct showCurrentDir = {"pwd", NULL, 0};
	struct command_struct makeDir = {"mkdir", NULL, 1};
	struct command_struct changeDir = {"cd", NULL, 1};
	struct command_struct copyFile = {"cp", NULL, 2};
	struct command_struct moveFile = {"mv", NULL, 2};
	struct command_struct deleteFile = {"rm", NULL, 1};
	struct command_struct displayFile = {"cat", NULL, 1};

	struct command_struct all_defined_commands[] = {listDir, showCurrentDir, makeDir, changeDir, copyFile, moveFile, deleteFile, displayFile};
	int command_struct_argument_size = 126;

	/* Allocate memory for command struct argument array */
	allocate_memory(&all_defined_commands, command_struct_argument_size);
	
	/* File handling variables */
	FILE* fp_out;
	FILE* fp_in;

	/* Determine what mode the project will run in */
	if(argc == 3 && strcmp(argv[1], "-f") == 0)
	{
		fp_in = fopen(argv[2], "r");
		fp_out = freopen("output.txt", "w", stdout);

		if(!fp_in)
		{
			fprintf(stderr, "Error opening input file %s\n", argv[2]);
			deallocate_memory(&all_defined_commands, command_struct_argument_size, &command_string);
			fclose(fp_out);
			exit(EXIT_FAILURE);
		}

		if(!fp_out)
		{
			fprintf(stderr, "Error opening output file\n");
			deallocate_memory(&all_defined_commands, command_struct_argument_size, &command_string);
			fclose(fp_out);
			exit(EXIT_FAILURE);
		}
		// redirecting stderr to stdout (output.txt)
		dup2(fileno(stdout), fileno(stderr));

		file_mode = true;
	}
	else if(argc != 1)
	{
		fprintf(stderr, "Error! Incorrect usage.\nUsage for filemode: ./pseudoshell -f <input_file_name.txt>\nUsage for user mode: ./pseudoshell\n");
		deallocate_memory(&all_defined_commands, command_struct_argument_size, &command_string);
		exit(EXIT_FAILURE);
	}
	
	/* Get commands from user/input file */
	while(exit_program == false) 
	{
		if(file_mode)
		{
			getline(&command_string, &bufsize, fp_in);

			// end of file for input
			if(feof(fp_in))
			{
				break;
			}
		}
		else
		{
			fprintf(stdout, ">>> ");
			getline(&command_string, &bufsize, stdin);

		}

		// special case: newline character with no command. Skip to next line.
		if(strcmp(command_string, "\n") == 0)
		{
			continue;
		}

		// remove newline character from command
		command_string[strlen(command_string) - 1] = '\0';

		/* tokenize command string */
		strtok_r_token = strtok_r(command_string, " ", &strtok_r_ptr);
		exit_input_parsing = false;
		while(exit_input_parsing == false)
		{
			if(strcmp(strtok_r_token, "exit") == 0)
			{
				exit_program = true;
				break;
			}
			/* Get command and arguments from user input */
			for(int i = 0; i < NUM_COMMANDS; i++)
			{
				// searching for a command match
				if(strcmp(all_defined_commands[i].name, strtok_r_token) == 0)
				{
					// getting arguments for command
					for(int j = 0; j < all_defined_commands[i].num_args; j++)
					{
						// get next token (after command)
						strtok_r_token = strtok_r(NULL, " ", &strtok_r_ptr);
						// too few arguments
						if(strtok_r_token == NULL || strcmp(strtok_r_token, ";") == 0)
						{
							fprintf(stderr, "Error! Unsupported parameters (too few parameters) for command: %s\n", all_defined_commands[i].name);
							exit_input_parsing = true;
							break;
						}
						strcpy(all_defined_commands[i].args[j], strtok_r_token);
					}
					// after obtaining arguments, we can only have either a semicolon or nothing as the next token. Additional token is too many parameters.
					strtok_r_token = strtok_r(NULL, " ", &strtok_r_ptr);
					// keep going if we haven't encountered an error
					if(!exit_input_parsing)
					{
						if(strtok_r_token == NULL)
						{
							// call command with arguments
							call_command(all_defined_commands[i]);
							exit_input_parsing = true;
							break;

						}
						else if(strcmp(strtok_r_token, ";") == 0)
						{
							call_command(all_defined_commands[i]);

							// move on to next command/token
							strtok_r_token = strtok_r(NULL, " ", &strtok_r_ptr);
							if(strtok_r_token == NULL)
							{
								strtok_r_token = "";
							}
						}
						else
						{
							/* Project specification delineates between syntax error due to no control code (two commands in succession with one another with no control code in between) and too many parameters */
							for(int j = 0; j < NUM_COMMANDS; j++)
							{
								if(strcmp(all_defined_commands[j].name, strtok_r_token) == 0)
								{
									fprintf(stderr, "Error! Incorrect syntax. No control code found.\n");
									exit_input_parsing = true;
									break;
								}
								else if(j == NUM_COMMANDS - 1)
								{
									fprintf(stderr, "Error! Unsupported parameters (too many parameters) for command: %s\n", all_defined_commands[i].name);
									exit_input_parsing = true;
									break;
								}
							}
						}
					}
					
					break; // once you have a match in command or are done because of an error no need to keep looking
				}
				/* user gave control code when there should have been a command - error */
				else if(strcmp(strtok_r_token, ";") == 0)
				{
						fprintf(stderr, "Error! Incorrect syntax. Expected command, but got control code.\n");
						exit_input_parsing = true;
						break;
				}
				/* command not in list of defined commands - error */
				else if(i == NUM_COMMANDS - 1)
				{
					fprintf(stderr, "Error! Unrecognized command: %s\n\n", strtok_r_token);
					exit_input_parsing = true;
					break;
				}
			}
			
		}
    }
	/* leave a newline at the end of the program */
	fprintf(stdout, "\n");

	/* Free the allocated memory */
	deallocate_memory(&all_defined_commands, command_struct_argument_size, &command_string);

	/* Close files */
	if(file_mode)
	{
		fclose(fp_in);
		fclose(fp_out);
	}

	/*exit from program */
	return EXIT_SUCCESS;
}
/*-----------------------------Program End-----------------------------------*/

/* Dynamically allocates memory for all defined command structs "argument" member consisting of 1 or more arguments
 * In other words, memory is allocated for arguments in their respective command structs.
 */
void allocate_memory(struct command_struct (*all_defined_commands)[], int command_struct_argument_size)
{
	for(int i = 0; i < NUM_COMMANDS; i++)
	{
		if((*all_defined_commands)[i].num_args > 0)
		{
			(*all_defined_commands)[i].args = (char**) malloc(sizeof(char*) * (*all_defined_commands)[i].num_args);
			for(int j = 0; j < (*all_defined_commands)[i].num_args; j++)
			{
				(*all_defined_commands)[i].args[j] = (char*) malloc(sizeof(char) * command_struct_argument_size);
			}
		}
	}

}

/* Frees memory for all defined command structs "argument" member consisting of 1 or more arguments 
 * and the buffer holding user input (command string). 
 * Command string must be freed because strtok_r dynamically allocates memory for it. 
 * */
void deallocate_memory(struct command_struct (*all_defined_commands)[], int command_struct_argument_size, char** command_string)
{
	free(*command_string);
	
	for(int i = 0; i < NUM_COMMANDS; i++)
	{
		if((*all_defined_commands)[i].num_args > 0)
		{
			for(int j = 0; j < (*all_defined_commands)[i].num_args; j++)
			{
				free((*all_defined_commands)[i].args[j]);
			}
			free((*all_defined_commands)[i].args);
		}
	}
}

/* Helper method to call appropriate command method
 * Compares the name in a command_struct to keynames
 * corresponding to methods in command.c. If a name matches,
 * The appropriate method in command.c is called.
 * */
void call_command(struct command_struct command)
{
	if(strcmp(command.name, "ls") == 0)
	{
		listDir();
	}
	else if(strcmp(command.name, "pwd") == 0)
	{
		showCurrentDir();
	}
	else if(strcmp(command.name, "mkdir") == 0)
	{
		makeDir(command.args[0]);
	}
	else if(strcmp(command.name, "cd") == 0)
	{
		changeDir(command.args[0]);
	}
	else if(strcmp(command.name, "cp") == 0)
	{
		copyFile(command.args[0], command.args[1]);
	}
	else if(strcmp(command.name, "mv") == 0)
	{
		moveFile(command.args[0], command.args[1]);
	}
	else if(strcmp(command.name, "rm") == 0)
	{
		deleteFile(command.args[0]);
	}
	else if(strcmp(command.name, "cat") == 0)
	{
		displayFile(command.args[0]);
	} 
	else
	{
		fprintf(stderr, "Error! Called unknown command.\n");
	}
}