/*
* Project 1: Pseudo-Shell: A single-threaded UNIX system command-line interface
* -----------------------------------------------------------------------------
* CIS 415: Operating Systems
*
* Author: Michael O'Connell
*
* Date: 4/20/2020
*
* Description:
* Mimics the command-line interface of a single-threaded UNIX system with
* the following UNIX-like commands:
* (1) ls
* (2) pwd
* (3) mkdir <name>
* (4) cd directory
* (5) cp <src> <dst> (files only)
* (6) mv <src> <dst>
* (7) rm <filename>
* (8) cat <filename>
* (9) exit
*
* Commands are performed with the following format: <cmd> <param1> <param2> ; <cmd2> ..
*
* There are 2 modes for the program:
* (1) interactive mode: Command-line interface, invoke by running the executable (./a.out)
* (2) file mode: read in an input file and output will be stored in "output.txt" (doesn't have to preexist)
*                invoke by running: ./a.out -f input.txt, with input.txt having commands formatted correctly

* Error checking for each command is limited to:
* (1) Incorrect parameters- either missing or too many (e.g. "cp <file>", missing a second)
* (2) Incorrect syntax
* 		(a) Missing or unrecognized command (e.g. "grep ./test", which isn't implemented)
*     (b) Repeated command without control code (e.g. "ls ls") OR ending a line on control code (e.g. "ls ;")
* (3) Some file error checking- not comprehensive and needs polish, see command.c for details
*
* Implementation in main():
* (1) User or file shell commands are collected using getline() in a main while loop.
*
* (2) Command is tokenized using strtok_r().
*
* (3) Flags are set according to the command being called (see flags_data (struct)) and are updated
*     during tokenization process. A flag with a value > 0 indicates that value should be expected during
*			the tokenization process (e.g. tokenizing cp <src> <dst>, will first yield a command flag, then once that's found
*			it will signal an argument flag with the value of 2, then a control flag indicating the command should be over).
*			This is the main source of error checking for (1) & (2) in error checking above.
*
*			The flags are
*			(a) argument: amount of arguments for a command, each time one is tokenized,
*										the field is decremented.
*			(b) command: one of the UNIX-like commands above (checked using validate_shell_command())
*     (c) control: either the end tokenization (NULL) or ";".
*
* (4) During the tokenization/flag verification process, command data is stored in command_data (struct)
*     When a token is correctly identified, its contents are stored accordingly. For example, if the commmand flag
*     is signaled and a command is verified, it's stored in the "command" field of command_data. This is the same
*     for arguments.
*
* (5) Once command data is collected successfully, it is used to call the appropriate shell command function.
*     function fields are supplied by command_data's arguments.
*
*
* To add a new command:
* (1) add it to the list of commands (cmd list)
* (2) update raise_cmd_flags() to include function.
* (3) update call_shell_command() to include function
* (4) add declaration to command.h
* (5) write implementation for function in command.c
*
* Notes (for grading):
* 1. In command.c, several non system call-related libraries are included
		 for error checking, string handling, and memory allocation.
		 I'm assuming memory allocation is OK in command.c from the homework (project 1 description) instruction:
		 "You must use dynamic memory allocation via ​ malloc()​ and ​free()​"
*/

/*-------------------------Preprocessor Directives---------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "command.h"
#include <unistd.h>
#include <fcntl.h>


/* shell command directives */
#define MAX_ARGS 2 // arguments for a shell command
#define MAX_CMDS 50 // sub-commands in a single shell command
#define MAX_CMD_LEN 100 // shell command names
#define MAX_ARG_LEN 100 // length of argument names for shell commands

/* Error message code directives */
#define UNRECOGNIZED_CMD_ERR 0
#define INCORRECT_SYNTAX_ERR 1
#define UNSUPPORTED_PARAM_ERR 2

/*---------------------------------------------------------------------------*/

// Holds shell command data that will be used
// to call apprpriate method.
typedef struct command_data_t
{
	char* command;
	char** arguments;
	unsigned int argument_amt;
} command_data;

// Holds flags program is expecting based
// on shell command input.
typedef struct flags_data_t
{
	unsigned int cmd, arg, cntrl;
} flags_data;

/* List of valid shell commands */
const char* shell_cmds[] = {
	"ls",
	"pwd",
	"mkdir",
	"cd",
	"cp",
	"mv",
	"rm",
	"cat"
};

void command_line_error_check(int argc, char* argv[]);
void print_usage();
int tokenize_shell_command(char** line, char**** cmd_list);
void allocate_command_data(command_data* command_d);
void free_command_data(command_data* command_d);
void init_command_data(command_data* command_d);
void shell_command_error_message(int code, char* cmd);
int validate_shell_command(char* cmd);
void raise_cmd_flags(char* cmd, flags_data* flags_d);
void call_shell_command(command_data* command_d);

/*-----------------------------Program Main----------------------------------*/
int main(int argc, char* argv[])
{
	setbuf(stdout, NULL);

	/* Main Function Variables */

	/* Allocate memory for the input buffer. */
	/*
	 * Lineptr and length are set to NULL and 0, respectively, before the
	 * getline call (main run loop).
	 *
	 * Under this circumstance, getline allocates a buffer for storing
	 * line. The buffer must be freed by the user program.
	 *
	 * Source: http://man7.org/linux/man-pages/man3/getline.3.html
	 */

	/* Invoking specified mode (interactive or file mode) */
	FILE *fp_stdout, *fp_stdin;
	unsigned int file_mode = 0;
	int fd_id;
	if(argc > 1 && strcmp(argv[1], "-f") == 0)
	{
		// redirecting input/output to appropriate files
		fp_stdin = freopen(argv[2], "r", stdin);
		fp_stdout = freopen("output.txt", "w", stdout);
		// redirect stderr to output.txt
		// necessary for the sake of file placeholders
		fd_id = dup2(fileno(stdout), fileno(stderr));
		file_mode = 1;
	}

	/* Error checking: command line arguments */
	// results in EXIT_FAILURE if invalid command line arguments
	command_line_error_check(argc, argv);

	/**** main run loop ****/
	// quit as a condition used in interactive mode
	command_data command_d;
	allocate_command_data(&command_d);
	flags_data flags_d;
	char* line = NULL;
	size_t len = 0;
	unsigned int quit = 0;
	int line_size;
	// for tokenizing input string (see for loop in main loop)
	char *str1, *token, *saveptr;
	unsigned int i;
	while(!quit)
	{
		/* Print >>> then get the input string */
		if(!file_mode)
		{
			fprintf(stdout, ">>> ");
		}

		// input from user (interactive mode) or input file (file mode)
		// keeping line size for line read failure
		line_size = getline(&line, &len, stdin);

		// getline returns -1 on failure to read a line
		// (including EOF condition for file mode)
		if(line_size == -1)
		{
			break;
		}
		// Special case: User entered nothing, UNIX behavior is to move
		// to the next line (e.g. <<< "")
		else if(line_size == 1)
		{
			continue;
		}

		/* Initalize flags struct */
		flags_d.arg = flags_d.cntrl = 0;
		// we're expecting a shelll command keyword first
		flags_d.cmd = 1;

		/* Initalize command_data struct */
		init_command_data(&command_d);

		for(str1 = line, i = 0; ;i++, str1 = NULL)
		{
			token = strtok_r(str1, " \n\t", &saveptr);

			// no more tokens
			if(token == NULL)
			{
				// raise an error depending on what we're expecting
				if(flags_d.arg)
				{
					// missing parameter error
					shell_command_error_message(UNSUPPORTED_PARAM_ERR, command_d.command);
					break;
				}
				else if(flags_d.cmd)
				{
					// Unrecognized command
					shell_command_error_message(UNRECOGNIZED_CMD_ERR, command_d.command);
					break;
				}
				// not argument or command, clean break
				else if(!flags_d.cmd && !flags_d.arg)
				{
					// call shell command
					call_shell_command(&command_d);
					init_command_data(&command_d);
					break;
				}

			}

			// expecting command
			else if(flags_d.cmd)
			{
				/* If the user entered <exit> then exit the loop */
				if(strcmp(token, "exit") == 0)
				{
					quit = 1;
					break;
				}

				// check that token is valid command
				// if so, add to command_d (struct) and set appropriate flags
				// if not, raise error (didn't recognize command)
				else if(validate_shell_command(token))
				{
					strcpy(command_d.command, token);
					raise_cmd_flags(command_d.command, &flags_d);
				}
				else
				{
					shell_command_error_message(UNRECOGNIZED_CMD_ERR, token);
					break;
				}
				// if so, shut command off and turn on correct flag
			}
			// expecting argument
			else if(flags_d.arg)
			{
				// missing a parameter, control code too early
				if(strcmp(token, ";") == 0)
				{
					shell_command_error_message(UNSUPPORTED_PARAM_ERR, command_d.command);
					break;
				}
				// repeated command
				else if(validate_shell_command(token))
				{
					shell_command_error_message(INCORRECT_SYNTAX_ERR, token);
					break;
				}
				// add to command_d + turn off/on correct flags
				strcpy(command_d.arguments[command_d.argument_amt], token);
				// arg-- used for if expecting more than 1 successive argument
				// e.g. "cp test here"
				flags_d.arg--;
				command_d.argument_amt++;

				// No more arguments to find, look for control
				if(flags_d.arg == 0)
				{
					flags_d.cntrl = 1;
				}
			}
			// expecting control code
			else if(flags_d.cntrl)
			{
				// command finished, call command
				if(strcmp(token, ";") == 0)
				{
					// call command
					call_shell_command(&command_d);
					init_command_data(&command_d);

					// turn off cntrl flag, turn on cmd
					flags_d.cntrl = 0;
					flags_d.cmd = 1;
					//flags_d.arg = 0;
				}
				else
				{
					if(validate_shell_command(token))
					{
						// repeated command with no control code
						shell_command_error_message(INCORRECT_SYNTAX_ERR, command_d.command);
						break;
					}
					// unsupported parameters (too many parameters)
					else
					{
						shell_command_error_message(UNSUPPORTED_PARAM_ERR, command_d.command);
						break;
					}
				}
			}
		}
	}

	/*Free the allocated memory & close files*/
	free(line);
	free_command_data(&command_d);
	if(file_mode)
	{
		fclose(fp_stdin);
		fclose(fp_stdout);
		close(fd_id);
	}

	return 1;
}

/* Provides error checking for command line arguments.
 *
 * Ensures file mode has correct number of arguments, correct flag, and input file
 * is valid.
 *
 * If an error is detected, specific detection is outputtted as well as
 * correct command line usage (see print_usage()).
 *
 * Return parameters:
 * None - returns if usage is correct, exits with EXIT_FAILURE if not.
 *
 *  */
void command_line_error_check(int argc, char* argv[])
{
	FILE *fp_input;

	// Correct arguments are only 2 options:
	// (1) interactive mode: ./pseudo-shell
	// (2) file mode: ./pseudo-shell -f <filename>
	// <filename> is the batch file we wish to read commands from.
	if(argc != 3 && argc != 1)
	{
		fprintf(stderr, "Incorrect number of arguments.\n");
	}
	// Checking for correct file mode arguments
	else if(argc == 3 && strcmp("-f", argv[1]) != 0)
	{
		fprintf(stderr, "Multiple arguments with no -f flag.\n");
	}
	// Checking that file exists
	else if(argc == 3 && strcmp("-f", argv[1]) == 0)
	{
		fp_input = fopen(argv[2], "r");

		if(fp_input == NULL)
		{
			fprintf(stderr, "File mode: Input file invalid.\n");
		}
		// file mode initiated
		else
		{
			fclose(fp_input);
			return;
		}
	}
	// interactive mode initiated
	else if(argc == 1)
	{
		return;
	}

  // directions on how to properly invoke program
	print_usage();

	exit(EXIT_FAILURE);
}

/* Helper function for command_line_error_check
 *
 * Outputs directions for how to invoke program correctly
 */
void print_usage()
{
	fprintf(stderr, "\nThere are 2 valid command line options for running this program:\n"
		"(1) interactive mode: ./pseudo-shell\n"
		"(2) file mode: ./pseudo-shell -f <filename>\n");
}


/* Helper method to allocate data for struct containing shell command data */
void allocate_command_data(command_data* command_d)
{
	int i;

	/* Allocating command data */
	command_d->command = (char*) malloc(sizeof(char) * MAX_CMD_LEN);
	command_d->arguments = (char**) malloc(sizeof(char*) * MAX_ARGS);

	for(i = 0; i < MAX_ARGS; i++)
	{
		command_d->arguments[i] = (char*) malloc(sizeof(char) * MAX_ARG_LEN);
	}
}

/* Helper method to free data for struct containing shell command data */
void free_command_data(command_data* command_d)
{
	int i;

	/* Freeing command data */
	for(i = 0; i < MAX_ARGS; i++)
	{
		free(command_d->arguments[i]);
	}
	free(command_d->arguments);
	free(command_d->command);
}

/* Helper method to initalize data for struct containing shell command data */
void init_command_data(command_data* command_d)
{
	strcpy(command_d->command, "");

	for(int i = 0; i < MAX_ARGS; i++)
	{
		strcpy(command_d->arguments[i], "");
	}

	command_d->argument_amt = 0;

}


/* Helper function to output generic shell command error message based on code
 *
 * Code key:
 * "0" = unrecognized command (e.g. "ls ; test", test is unrecognized)
 * "1" = Incorrect syntax for a command (e.g. "ls ls")
 * "2" = Unsupported parameters for a command (e.g. "ls test", ls doesn't support parameters)
 *
 * Return parameters:
 * None
*/
void shell_command_error_message(int code, char* cmd)
{
	switch(code)
	{
		case 0:
			fprintf(stderr, "Error! Unrecognized command: %s\n", cmd);
			break;
		case 1:
			fprintf(stderr, "Error! Incorrect syntax. No control code found.\n");
			break;
		case 2:
			fprintf(stderr, "Error! Unsupported parameters for command: %s\n", cmd);
			break;
		default:
			fprintf(stderr, "Error code: %d not supported. Command called: %s\n", code, cmd);
			break;
	}

	// output a new line at the end of all error messages
	fprintf(stderr, "\n");

}

/* Method to validate inputted shell command against list of valid shell commands
 *
 * Return parameters:
 * Returns 1 if command is valid, 0 if not*/
int validate_shell_command(char* cmd)
{
	unsigned int cmd_cnt = (sizeof(shell_cmds) / sizeof(shell_cmds[0]));

	/* Check for correct command */
	unsigned int i;
	for(i = 0; i < cmd_cnt; i++)
	{
		// found a match
		if(strcmp(cmd, shell_cmds[i]) == 0)
		{
			return 1;
		}
	}

	// didn't find a match
	return 0;
}

/* raises appropriate flags in flags_d for specific shell commands
 *
 * See shell_cmds in global space for full list of commands
 *
 * Examples:
 * - ls should have all flags turned off(all set to 0)
 * - mdkir should set arg to 1 & arg_position to 1
 *
 * Return parameter:
 * None
 */
void raise_cmd_flags(char* cmd, flags_data* flags_d)
{
	// ls
	if(strcmp(cmd, shell_cmds[0]) == 0)
	{
		flags_d->cmd = 0;
		flags_d->arg = 0;
		flags_d->cntrl = 1;
	}
	// pwd
	else if(strcmp(cmd, shell_cmds[1]) == 0)
	{
		flags_d->cmd = 0;
		flags_d->arg = 0;
		flags_d->cntrl = 1;
	}
	// mkdir
	else if(strcmp(cmd, shell_cmds[2]) == 0)
	{
		flags_d->cmd = 0;
		flags_d->arg = 1;
		flags_d->cntrl = 0;
	}
	// cd
	else if(strcmp(cmd, shell_cmds[3]) == 0)
	{
		flags_d->cmd = 0;
		flags_d->arg = 1;
		flags_d->cntrl = 0;
	}
	// cp
	else if(strcmp(cmd, shell_cmds[4]) == 0)
	{
		flags_d->cmd = 0;
		flags_d->arg = 2;
		flags_d->cntrl = 0;
	}
	// mv
	else if(strcmp(cmd, shell_cmds[5]) == 0)
	{
		flags_d->cmd = 0;
		flags_d->arg = 2;
		flags_d->cntrl = 0;
	}
	// rm
	else if(strcmp(cmd, shell_cmds[6]) == 0)
	{
		flags_d->cmd = 0;
		flags_d->arg = 1;
		flags_d->cntrl = 0;
	}
	// cat
	else if(strcmp(cmd, shell_cmds[7]) == 0)
	{
		flags_d->cmd = 0;
		flags_d->arg = 1;
		flags_d->cntrl = 0;
	}
	else
	{
		fprintf(stderr, "Attempt to raise command flags failed: command not supported.\n");
		// exit program, other error checking mechanisms also failed
		exit(EXIT_FAILURE);
	}
}

/* Calls apprpriate shell command using data from command_data (struct)
 *
 * Tokenization process in main loop gathers necessary shell command data
 * (which is stored in command_data).
 *
 * Method uses data to call requested shell command using command_data.command,
 * and supply necessary arguments for shell command function call using command_data.arguments[].
 *
 * Return parameter:
 * None
 */
void call_shell_command(command_data* command_d)
{
	// ls
	if(strcmp(command_d->command, shell_cmds[0]) == 0)
	{
		listDir();
	}
	// pwd
	else if(strcmp(command_d->command, shell_cmds[1]) == 0)
	{
		showCurrentDir();
	}
	// mkdir
	else if(strcmp(command_d->command, shell_cmds[2]) == 0)
	{
		makeDir(command_d->arguments[0]);
	}
	// cd
	else if(strcmp(command_d->command, shell_cmds[3]) == 0)
	{
		changeDir(command_d->arguments[0]);
	}
	// cp
	else if(strcmp(command_d->command, shell_cmds[4]) == 0)
	{
		copyFile(command_d->arguments[0], command_d->arguments[1]);
	}
	// mv
	else if(strcmp(command_d->command, shell_cmds[5]) == 0)
	{
		moveFile(command_d->arguments[0], command_d->arguments[1]);
	}
	// rm
	else if(strcmp(command_d->command, shell_cmds[6]) == 0)
	{
		deleteFile(command_d->arguments[0]);
	}
	// cat
	else if(strcmp(command_d->command, shell_cmds[7]) == 0)
	{
		displayFile(command_d->arguments[0]);
	}
	else
	{
		fprintf(stderr, "Attempt to call shell command failed: command not supported.\n");
		// exit program, other error checking mechanisms also failed
		exit(EXIT_FAILURE);
	}

}
/*-----------------------------Program End-----------------------------------*/
