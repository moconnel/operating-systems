/*
* CIS 415: Operating Systems
*
* Author: Michael O'Connell
*
* Date: 4/21/2020
*
* Description:
* All UNIX-like commands detailed in main.c are implemented (primarily with system calls) here.
*
* Detailed implementation notes are given in every function.
*
* Note:
* Because the homework directions call for minimal non system call-related libraries,
* some implementations could be more elegant (such as getting the correct filepaths for mv and cp)
*/

// for malloc, realloc, string handling, & error checking
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
// system call libraries
#include <unistd.h>
#include <sys/types.h>
#include <sys/sendfile.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>

/* file directives */
#define MAX_CHAR 80
#define MAX_FILES 70

/* Mimics Unix "ls" command:
 * Shows the names of files and folders in current directory
 *
 * Implementation steps:
 * (1) get pathname of current directory
 * (2) open current directory stream (using pathname)
 * (3) read each directory entry
 * (4) output contents of directory entry using write, with 1 = stdout
 *
 * Note:
 * - There is minimal error checking, to improve check for NULL pointer
 *   after each system call (read Linux man pages for specific errors).
 *
 * - Due to readdir() behavior, order of files/directories is undefined
     (specifically when each dir_entry is accessed).
 *   If alphabetical order is desired, use a sorting algorithm to implement.
*/
void listDir()
{
  /* (1) get pathname of current directory */
  // buffer to hold pathname
  unsigned int max_char = 80;
  char* working_dir_path = (char*) malloc(sizeof(char) * max_char);

  // Error handling for pathway:
  // if length of path exceeds max_char, NULL is returned
  // reallocate as necessary
  // source: http://man7.org/linux/man-pages/man2/getcwd.2.html
  // path will be stored in working_dir_path
  while(!getcwd(working_dir_path, max_char))
  {
    working_dir_path = realloc(working_dir_path, max_char * 2);
  }

  /* (2) open current directory stream */
  DIR* dir_ptr = opendir(working_dir_path);

  /* (3) reading directory entries (output to stdout) */
  struct dirent* dir_entry;

  // returns NULL on EOF or error
  dir_entry = readdir(dir_ptr);

  while(dir_entry)
  {
    // stdout = 1
    // concatenate a space after each listing,
    // strlen used to reflect bytes in listing + one space
    write(1, strcat(dir_entry->d_name, " "), strlen(dir_entry->d_name) + 1);
    dir_entry = readdir(dir_ptr);
  }
  // after listings, output newline
  write(1, "\n", 1);

  free(working_dir_path);
  closedir(dir_ptr);
}

/* Mimics Unix pwd command
 * Outputs current directory
 *
 * Implementation steps:
 * (1) get pathname of current directory.
 * (2) output pathname (using write)
 *
 */
void showCurrentDir()
{
  /* (1) get pathname of current directory */
  // buffer to hold pathname
  unsigned int max_char = 80;
  char* working_dir_path = (char*) malloc(sizeof(char) * max_char);

  // Error handling for pathway:
  // if length of path exceeds max_char, NULL is returned
  // reallocate as necessary
  // source: http://man7.org/linux/man-pages/man2/getcwd.2.html
  // path will be stored in working_dir_path
  while(!getcwd(working_dir_path, max_char))
  {
    working_dir_path = realloc(working_dir_path, max_char * 2);
  }

  /* (2) output current directory path */
  write(1, working_dir_path, strlen(working_dir_path));
  write(1, "\n", 1);

  free(working_dir_path);
}

/* Mimics Unix mkdir command
 * Creates new directory with the filename specified by dirName
 *
 * Implementation steps:
 * (1) Use mkdir (system call) to create new directory
 *
 * Mode field indicates:
 * S_IRWXU owner has read, write, and execute permission
 * S_IRWXG group has read, write, and execute permission
 * S_IROTH others have read permission
 * S_IXOTH others have execute permission
 */
void makeDir(char *dirName)
{
  int success;
  char* msg = "Error: mkdir failed.\n";

  success = mkdir(dirName, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  if(success == -1)
  {
    write(2, msg, strlen(msg));
    write(2, "\n", 1);
  }
}

/* Mimics Unix cd command
 *
 * Changes current working directory (for current process)
 *
 * Implementation steps:
 * (1) use chdir (system call) to change current working directory
 */
void changeDir(char *dirName)
{
  int success;
  char* msg = "Error: cd failed (probably not a valid directory).\n";

  success = chdir(dirName);

  if(success == -1)
  {
    write(2, msg, strlen(msg));
    write(2, "\n", 1);
  }
}

/* Mimics the Unix cp command
 * Copies source file path to destination file path.
 *
 * Implementation steps:
 * (1) Validate src file exists
 * (2) get src file permissions
 * (3) create destination file (or open existing specified destination file)
 * (4) transfer src file data to dst file
 */
void copyFile(char *sourcePath, char *destinationPath)
{
  int src, dst;
  char* src_err_msg = "Error: cp failed (not a valid source file).\n";
  char* src_err_msg_perm = "Error: cp failed (failed to get source file permissions).\n";
  char* dst_err_msg_perm = "Error: cp failed (failed to get destination file permissions).\n";
  char* dst_err_msg = "Error: cp failed (failed to create or open destination file).\n";

  /* Validate src file exists */
  if((src = open(sourcePath, O_RDONLY)) == -1)
  {
    write(2, src_err_msg, strlen(src_err_msg));
    write(2, "\n", 1);
    return;
  }
  // get permissions for source file
  struct stat file_stat;
  struct stat file_stat_d;
  if(stat(sourcePath, &file_stat) < 0)
  {
    write(2, src_err_msg_perm, strlen(src_err_msg_perm));
    write(2, "\n", 1);
    close(src);
    return;
  }

  // re-configure path for directories - statement tests whther source is valid and
  // whether source is directory
  char src_name[MAX_CHAR];
  if(stat(destinationPath, &file_stat_d) == 0 && S_ISDIR(file_stat_d.st_mode))
  {
    // change path name appropriately
    // e.g. <<< cp test.txt ..
    // will replace .. with ../test.txt
    // so rename will work correctly
    // implmentation is to find what index '/' exists in sourcePath
    // and copy over everything after it to a new char array
    //
    // note: a more elegant solution is to use basename() in libgen.h
    // but outside libraries aren't allowed for this project

    // (1) get filename from sourcePath
    unsigned int sourcePath_len = strlen(sourcePath);
    int filename_index = -1;
    unsigned int src_name_index;
    unsigned int i;
    for(i = sourcePath_len - 1; i > 0; i--)
    {
      // first instance of '/' (linux), filename is after
      if(sourcePath[i] == '/')
      {
        filename_index = i + 1;
        break;
      }
    }

    // (2) copy filename over, with null terminated byte
    // if filename_index was never updated, there's no "/"
    // in sourcePath
    if(filename_index != -1)
    {
      src_name_index = 0;
      for(i = filename_index; i < sourcePath_len + 1; i++)
      {
        src_name[src_name_index] = sourcePath[i];
        src_name_index++;
      }
    }
    // file_name_index was never updated, stick with sourcePath
    // avoids two seperate strcat sequences (see below)
    else
    {
      strcpy(src_name, sourcePath);
    }

    strcat(destinationPath, "/");
    strcat(destinationPath, src_name);
  }

  /* Create dst file (or overwrite is exists) */
  // notes:
  // - creat is open() with O_CREAT|O_WRONLY|O_TRUNC flags
  // - using the same permissions as source
  if((dst = creat(destinationPath, file_stat.st_mode)) == -1)
  {
    write(2, dst_err_msg, strlen(dst_err_msg));
    write(2, "\n", 1);
    close(src);
    return;
  }

  /* Transfer src data to dst */
  // use bytes_copied for debugging if useful
  off_t bytes_copied = 0;
  sendfile(dst, src, &bytes_copied, file_stat.st_size);

  close(src);
  close(dst);

}

/* Mimics the Unix mv command
 * Functionality is limited to moving files.
 * Moves file from the location specified by sourcePath to the location
 * specified by destinationPath.
 *
 * Implementation steps:
 * (1) Determine if destinationPath is a non-explicit directory (e.g. mv test.txt ..)
 *     If so, re-configure path.
 * (2) Use rename linux system call to move sourcePath to destinationPath
 */
void moveFile(char *sourcePath, char *destinationPath)
{
  char* err_msg = "Error: mv failed.\n";
  int success;
  struct stat file_stat;
  struct stat file_stat_d;

  // re-configure path for directories - statement tests whther source is valid and
  // whether source is directory
  char src_name[MAX_CHAR];
  if(stat(destinationPath, &file_stat_d) == 0 && S_ISDIR(file_stat_d.st_mode))
  {
    // change path name appropriately
    // e.g. <<< cp test.txt ..
    // will replace .. with ../test.txt
    // so rename will work correctly
    // implmentation is to find what index '/' exists in sourcePath
    // and copy over everything after it to a new char array
    //
    // note:
    // a more elegant solution is to use basename() in libgen.h
    // but outside libraries aren't allowed for this project

    // (1) get filename from sourcePath
    unsigned int sourcePath_len = strlen(sourcePath);
    int filename_index = -1;
    unsigned int src_name_index;
    unsigned int i;
    for(i = sourcePath_len - 1; i > 0; i--)
    {
      // first instance of '/' (linux), filename is after
      if(sourcePath[i] == '/')
      {
        filename_index = i + 1;
        break;
      }
    }

    // (2) copy filename over, with null terminated byte
    // if filename_index was never updated, there's no "/"
    // in sourcePath
    if(filename_index != -1)
    {
      src_name_index = 0;
      for(i = filename_index; i < sourcePath_len + 1; i++)
      {
        src_name[src_name_index] = sourcePath[i];
        src_name_index++;
      }
    }
    // file_name_index was never updated, stick with sourcePath
    // avoids two seperate strcat sequences (see below)
    else
    {
      strcpy(src_name, sourcePath);
    }

    strcat(destinationPath, "/");
    strcat(destinationPath, src_name);
  }

  success = rename(sourcePath, destinationPath);

  if(success < 0)
  {
    write(2, err_msg, strlen(err_msg));
    write(2, "\n", 1);
    return;
  }
}

/* Unix the rm command
 * Only for removing files (not directories).
 * Removes specified file from the current directory
 *
 * Implementation steps:
 * (1) Implemented using the unlink() system call
 */
void deleteFile(char *filename)
{
  char* err_msg = "Error: rm failed (most likely wrong filename).\n";
  int success;

  success = unlink(filename);

  if(success < 0)
  {
    write(2, err_msg, strlen(err_msg));
    write(2, "\n", 1);
    return;
  }
}

/* Mimics Unix cat command
*  Displays file's output to stdout
*
*  Implementation steps:
*  (1) open file
*  (2) read file contents to buffer
*  (3) write buffer to stdout
*/
void displayFile(char *filename)
{
  int file_dst;
  char* err_open_msg = "Error: cat failed (failed to open file).\n";
  char* err_data_msg = "Error: cat failed (failed to get file data).\n";

  /* Open file */
  file_dst = open(filename, O_RDONLY);

  if(!file_dst)
  {
    write(2, err_open_msg, strlen(err_open_msg));
    write(2, "\n", 1);
    return;
  }

  // get size for file
  struct stat file_stat;
  if(stat(filename, &file_stat) < 0)
  {
    write(2, err_data_msg, strlen(err_data_msg));
    write(2, "\n", 1);
    close(file_dst);
    return;
  }

  /* read contents from a file to buffer */
  char buf[file_stat.st_size];
  read(file_dst, buf, file_stat.st_size);

  /* read contents of file (buffer) to stdout */
  write(1, buf, file_stat.st_size);

  close(file_dst);
}
