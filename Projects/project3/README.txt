I. Usage
- Makefile produces an executable, server. 
  To run: ./server input.txt

II. Tests
- input_full_publisher_test.txt 
  successfully pushes all topic entries without dequeuing.

- pub_dont_give_up_.txt
  Successfully continues to push all topic entries before dequeing
  after 30 seconds (delta = 30)

- sub_empty_test.txt
  Usuccessfully attempts to get topic entries from an empty queue.
  Program exits normally.

- normal_test (directory)
  Screenshots from given project input.