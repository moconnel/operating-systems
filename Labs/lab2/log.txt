==30884== Memcheck, a memory error detector
==30884== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==30884== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==30884== Command: ./main2.out file_input2.txt
==30884== 
==30884== 
==30884== HEAP SUMMARY:
==30884==     in use at exit: 0 bytes in 0 blocks
==30884==   total heap usage: 5 allocs, 5 frees, 9,416 bytes allocated
==30884== 
==30884== All heap blocks were freed -- no leaks are possible
==30884== 
==30884== For counts of detected and suppressed errors, rerun with: -v
==30884== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
