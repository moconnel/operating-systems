#!/bin/bash
set -x #echo on
gcc -g -o main.out main.c 
./main.out
gcc -g -o main2.out main2.c
./main2.out file_input.txt
cat output.txt
./main2.out file_input2.txt
cat output.txt
valgrind --leak-check=full --tool=memcheck ./main2.out file_input2.txt > log.txt 2>&1
cat log.txt
screenfetch