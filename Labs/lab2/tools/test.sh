#!/bin/bash
set -x #echo on
echo -e "*** USER INPUT ***\n"
gcc -o main.out main.c
./expect #run program, mimic user input, you may need to install expect
./expect2
echo -e "*** FILE INPUT ***\n"
gcc -o main2.out main2.c
./main2.out file_input.txt
cat output.txt
./main2.out file_input2.txt
cat output.txt