Lab5 is built on top of lab4. 

The Makefile provided creates an executable, lab5, with the "make" command. It deletes the executable using the "make clean" command.
Run lab5 with 5 child processes using the command: ./lab5 5
You can use the same debugging command from lab4: ./lab5 5 | gnome-terminal -- bash top_script.sh