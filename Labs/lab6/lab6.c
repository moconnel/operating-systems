/*
Author: Michael O'Connell
Assignment: Lab 6 - Implementing a collection of queues in C
Class: CIS 415 - Operating Systems at University of Oregon, Fall 2020
Date: 11/20/2020

Description:
For this lab, we will first implement a single queue for the meal ticket struct. This will allow us
to push/pop meal tickets onto/from the queue. Once we have completed this, we will implement
a registry for meal tickets for a restaurant that contains a queues for the following course types:
Breakfast, Lunch, Dinner, Bar. This will allow waiters to electronically push orders into the
registry for the chefs to pull and complete.

Implementation:
1. Initialize registry (list of circular ring buffers).
2. Create MTQs for Breakfast, Lunch, Dinner, and Bar.
3. Create and push 3 meal tickets onto each queue.
4. Create and push a 4th meal ticket to demonstrate enqueue() can handle
   a full queue appropriately.
5. Pop a meal ticket from each queue and display it until all queues are empty.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME 126
#define MAX_QUEUES 10

struct meal_ticket 
{
    int ticket_num;
    char* dish_name;
};

struct MTQ 
{
    char name[MAX_NAME];
    struct meal_ticket* const buffer;
    int head;
    int tail;
    int current_length;
    const int max_length;
};

struct MTQ* registry[MAX_QUEUES];

int enqueue(char* MTQ_ID, struct meal_ticket* MT);
int dequeue(char* MTQ_ID, struct meal_ticket* MT);

int main() 
{
    /* Initalize the registry */
    for(int i = 0; i < MAX_QUEUES; i++)
    {
        registry[i] = NULL;
    }

    /* 1a. Create MTQs */
    int num_mtqs = 4;
    int num_meal_tickets = 4;
    char* mtq_names[MAX_NAME] = {"Breakfast, Lunch, Dinner, Bar"};
    struct MTQ breakfast = {"Breakfast", (struct meal_ticket*) malloc(sizeof(struct meal_ticket) * 3), 0, 0, 0, 3};
    struct MTQ lunch = {"Lunch", (struct meal_ticket*) malloc(sizeof(struct meal_ticket) * 3), 0, 0, 0, 3};
    struct MTQ dinner = {"Dinner", (struct meal_ticket*) malloc(sizeof(struct meal_ticket) * 3), 0, 0, 0, 3};
    struct MTQ bar = {"Bar", (struct meal_ticket*) malloc(sizeof(struct meal_ticket) * 3), 0, 0, 0, 3};

    /* 1a. Push MTQs to the registry */
    registry[0] = &breakfast;
    registry[1] = &lunch;
    registry[2] = &dinner;
    registry[3] = &bar;

    /* 2. Create and push 3 meal tickets into each queue */
    // breakfast
    struct meal_ticket mt0 = {-1, "eggs"};
    struct meal_ticket mt1 = {-1, "bacon"};
    struct meal_ticket mt2 = {-1, "toast"};
    struct meal_ticket mt3 = {-1, "Grapefruit"};
    enqueue("Breakfast", &mt0);
    enqueue("Breakfast", &mt1);
    enqueue("Breakfast", &mt2);
    enqueue("Breakfast", &mt3);

    // lunch
    struct meal_ticket mt4 = {-1, "burger"};
    struct meal_ticket mt5 = {-1, "chicken"};
    struct meal_ticket mt6 = {-1, "sandwich"};
    struct meal_ticket mt7 = {-1, "fries"};
    enqueue("Lunch", &mt4);
    enqueue("Lunch", &mt5);
    enqueue("Lunch", &mt6);
    enqueue("Lunch", &mt7);

    // dinner
    struct meal_ticket mt8 = {-1, "risotto"};
    struct meal_ticket mt9 = {-1, "scallops"};
    struct meal_ticket mt10 = {-1, "beef cheek"};
    struct meal_ticket mt11 = {-1, "spaghetti"};
    enqueue("Dinner", &mt8);
    enqueue("Dinner", &mt9);
    enqueue("Dinner", &mt10);
    enqueue("Dinner", &mt11);

    // bar
    struct meal_ticket mt12 = {-1, "wine"};
    struct meal_ticket mt13 = {-1, "beer"};
    struct meal_ticket mt14 = {-1, "mixed drink"};
    struct meal_ticket mt15 = {-1, "soda"};
    enqueue("Bar", &mt12);
    enqueue("Bar", &mt13);
    enqueue("Bar", &mt14);
    enqueue("Bar", &mt15);

    /* Pop a meal ticket from each queue and display it until all queues are empty */
    char curr_mtq[MAX_NAME];
    for(int i = 0; i < num_mtqs; i++)
    {
        strcpy(curr_mtq, registry[i]->name);
        struct meal_ticket empty_mt = {-1, (char*) malloc(sizeof(char) * MAX_NAME)};
        memset(empty_mt.dish_name, '\0', MAX_NAME);
        for(int j = 0; j < num_meal_tickets; j++)
        {
            dequeue(curr_mtq, &empty_mt);
        }
        free(empty_mt.dish_name);
    }

    /* Deallocate MTQ buffers */
    free(breakfast.buffer);
    free(lunch.buffer);
    free(dinner.buffer);
    free(bar.buffer);

    return 0;
}

int enqueue(char* MTQ_ID, struct meal_ticket* MT)
{
    /* Find desired MTQ using MTQ_ID */
    struct MTQ* MTQ_target;
    for(int i = 0; i < MAX_QUEUES; i++)
    {
        if(registry[i] != NULL && strcmp(MTQ_ID, registry[i]->name) == 0)
        {
            MTQ_target = registry[i];
            break;
        }
        // Didn't find desired MTQ
        else if(i == MAX_QUEUES - 1)
        {
            printf("Enqueue Error: Could not find desired MTQ.\n");
            return 0;

        }
    }

    /* Check whether MTQ_target is full */
    if(MTQ_target->head == MTQ_target->tail && 
       MTQ_target->current_length == MTQ_target->max_length)
    {
        printf("pushing: Queue: %s - Error Queue is full.\n", MTQ_target->name);
        return 0;
    }

    /* Assign ticketnum to tickets position in the queue */
    MT->ticket_num = MTQ_target->current_length;

    /* Push meal ticket onto MTQ_target queue */
    MTQ_target->buffer[MTQ_target->head] = *MT;
    MTQ_target->current_length++;

    /* Increment head pointer so it always points to the 
       position after the newest element in the queue 
    */
    if(MTQ_target->head < MTQ_target->max_length - 1)
    {
        MTQ_target->head++;
    }
    else
    {
        MTQ_target->head = 0;
    }

    printf("pushing: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_target->name, MT->ticket_num, MT->dish_name);
    return 1;
}

int dequeue(char* MTQ_ID, struct meal_ticket* MT)
{
    /* Find desired MTQ using MTQ_ID */
    struct MTQ* MTQ_target;
    for(int i = 0; i < MAX_QUEUES; i++)
    {
        if(registry[i] != NULL && strcmp(MTQ_ID, registry[i]->name) == 0)
        {
            MTQ_target = registry[i];
            break;
        }
        // Didn't find desired MTQ
        else if(i == MAX_QUEUES - 1)
        {
            printf("Enqueue Error: Could not find desired MTQ.\n");
            return 0;
        }
    }

    /* Check if MTQ_target is empty */
    if((MTQ_target->head == MTQ_target->tail && 
      MTQ_target->current_length == 0))
    {
        printf("popping: Queue %s - Queue is empty, nothing to pop\n", MTQ_target->name);
        return 0;
    }

    /* Get info to fill empty meal ticket */
    strcpy(MT->dish_name, MTQ_target->buffer[MTQ_target->tail].dish_name);
    MT->ticket_num = MTQ_target->buffer[MTQ_target->tail].ticket_num;

    /* Pop from MTQ_target - 
       Increment tail pointer so it always points to the 
       position after the newest element in the queue 
    */
    if(MTQ_target->tail < MTQ_target->max_length - 1)
    {
        MTQ_target->tail++;
    }
    else
    {
        MTQ_target->tail = 0;
    }

    MTQ_target->current_length--;

    printf("popping: Queue: %s - Ticket Number: %d - Dish: %s\n", MTQ_target->name, MT->ticket_num, MT->dish_name);
    return 1;
}