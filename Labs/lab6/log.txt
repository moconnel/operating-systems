==20917== Memcheck, a memory error detector
==20917== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==20917== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==20917== Command: ./lab6
==20917== 
pushing: Queue: Breakfast - Ticket Number: 0 - Dish: eggs
pushing: Queue: Breakfast - Ticket Number: 1 - Dish: bacon
pushing: Queue: Breakfast - Ticket Number: 2 - Dish: toast
pushing: Queue: Breakfast - Error Queue is full.
pushing: Queue: Lunch - Ticket Number: 0 - Dish: burger
pushing: Queue: Lunch - Ticket Number: 1 - Dish: chicken
pushing: Queue: Lunch - Ticket Number: 2 - Dish: sandwich
pushing: Queue: Lunch - Error Queue is full.
pushing: Queue: Dinner - Ticket Number: 0 - Dish: risotto
pushing: Queue: Dinner - Ticket Number: 1 - Dish: scallops
pushing: Queue: Dinner - Ticket Number: 2 - Dish: beef cheek
pushing: Queue: Dinner - Error Queue is full.
pushing: Queue: Bar - Ticket Number: 0 - Dish: wine
pushing: Queue: Bar - Ticket Number: 1 - Dish: beer
pushing: Queue: Bar - Ticket Number: 2 - Dish: mixed drink
pushing: Queue: Bar - Error Queue is full.
popping: Queue: Breakfast - Ticket Number: 0 - Dish: eggs
popping: Queue: Breakfast - Ticket Number: 1 - Dish: bacon
popping: Queue: Breakfast - Ticket Number: 2 - Dish: toast
popping: Queue Breakfast - Queue is empty, nothing to pop
popping: Queue: Lunch - Ticket Number: 0 - Dish: burger
popping: Queue: Lunch - Ticket Number: 1 - Dish: chicken
popping: Queue: Lunch - Ticket Number: 2 - Dish: sandwich
popping: Queue Lunch - Queue is empty, nothing to pop
popping: Queue: Dinner - Ticket Number: 0 - Dish: risotto
popping: Queue: Dinner - Ticket Number: 1 - Dish: scallops
popping: Queue: Dinner - Ticket Number: 2 - Dish: beef cheek
popping: Queue Dinner - Queue is empty, nothing to pop
popping: Queue: Bar - Ticket Number: 0 - Dish: wine
popping: Queue: Bar - Ticket Number: 1 - Dish: beer
popping: Queue: Bar - Ticket Number: 2 - Dish: mixed drink
popping: Queue Bar - Queue is empty, nothing to pop
==20917== 
==20917== HEAP SUMMARY:
==20917==     in use at exit: 0 bytes in 0 blocks
==20917==   total heap usage: 9 allocs, 9 frees, 4,792 bytes allocated
==20917== 
==20917== All heap blocks were freed -- no leaks are possible
==20917== 
==20917== For counts of detected and suppressed errors, rerun with: -v
==20917== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
