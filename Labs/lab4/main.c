/* 
    Author: Michael O'Connell
    Date: 10/29/20
    Class: Operating Systems at University of Oregon, Fall 20'
    
    Description: 
    The goal of lab4 is to create a controlled number of processes 
    and use them to execute other programs.
    
    Specically in this lab we:
    1. Ask user for the number of processes via the command line.
    2. Create a pid_t array of the requested size.
    3. Start the exact number of processes.
    4. Wait for all the children processes to finish.

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>

void script_print (pid_t* pid_ary, int size);


int main(int argc, char* argv[])
{
    /* Get number of processes via command line */
    int num_process;
    num_process = atoi(argv[1]);

    if(num_process < 1) 
    {
        fprintf(stderr, "Cannot create less than 1 process.\n");
        exit(EXIT_FAILURE);
    }

    /* Create an array of pid_t */
    pid_t process_array[num_process];

    /* Initialize process array */
    for(int i = 0; i < num_process; i++)
    {
        process_array[i] = 0;
    }

    /* Utilize a loop to start the exact number of processes */
    char* arg[2] = {"-seconds 5", NULL};
    int test;

    for(int i = 0; i < num_process; i++)
    {
        process_array[i] = fork();

        // child process
        if(process_array[i] == 0)
        {
            // child process' inherited process address space (via fork()) is 
            // ovewritten with new program (the executable, iobound) via execvp
            execvp("./iobound", arg);
        }

        // For debugging
        script_print(process_array, num_process); 

    }

    /* Wait for all the children processes to finish 

       Note that because the child processes are running
       a different program, we only have the parent process
       by this point of execution in our code. Therefore, we
       can simply call waitpid() once for each parent process id
       without worrying about if we're calling it on a child process.
       In other words, the array at this point in execution is the
       just from the parent process.    
    */
    for(int i = 0; i < num_process; i++)
    {
        waitpid(process_array[i], NULL, 0);
    }

    return 0;

}

/* Helper function given for debugging. Creates a script ("top_script.sh")
   that is then used to call "top" for all the parent processes via the shell command:
   "./lab4 3 | gnome-terminal -- bash top_script.sh"

   Alternatively, another way to debug is to execute the program normally,
   call top from another teminal window and observe the amount of processes running.
   
   Note that "iobound" and "cpubound" are cpu-heavy process only created for
   observing processes running in real time (via some form of "top").
*/

void script_print (pid_t* pid_ary, int size)
{
    FILE* fout;
    fout = fopen ("top_script.sh", "w");
    fprintf(fout, "#!/bin/bash\ntop");
    for (int i = 0; i < size; i++)
    {
        fprintf(fout, " -p %d", (int)(pid_ary[i]));
    }
    fprintf(fout, "\n");
    fclose (fout);
}