/*
* Description: String processing in C 
*
* Author: Michael O'Connell
*
* Date: 10/3/20
*
* The goal of this program is to convert command input into "tokens" and output 
* the tokens sequentially. 
* An example would be the commmand: "mkdir ; ls" 
* when tokenized would be:
* T0: mkdir
* T1: ;
* T2: ls
*
* The system generally works by:
* 1. Getting command from user/file.
* 2. Begin tokenization process via strtok_r().
* 3. Output the command (token).
* 4. Repeat 2-4 until all tokens are outputted.
* 5. Repeat starting from 1 if there are more commands.

*/
/*-------------------------Preprocessor Directives---------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "command.h"

/*---------------------------------------------------------------------------*/

/*-----------------------------Program Main----------------------------------*/
int main(int argc, char** argv) {
	/* file handling variables */
	FILE* fp_out = fopen("output.txt", "w");

	/* string handling variables */
	size_t bufsize = 0;
	char* command_string = NULL; // for getline() to allocate memory
	char* strtok_r_ptr;
	char* strtok_r_token;

	/* program/system variables */
	bool exit_flag = false;
	int token_number;
	
	/* Get commands from file */
	while(exit_flag == false) 
	{
		// get command from each file line 
		getline(&command_string, &bufsize, fp_in);

		// rcheck if fp has reached EOF. If so one more token then exit
		if(feof(fp_in))
		{
			exit_flag = true;
		}

		// special case: newline character with no command. Skip to next line.
		if(strcmp(command_string, "\n") == 0)
		{
			continue;
		}
		
		/* tokenize command string, display token and token number */
		token_number = 0;
		strtok_r_token = strtok_r(command_string, " ", &strtok_r_ptr);
		while(strtok_r_token != NULL)
		{
			// check if exit command has been called
			if(strcmp(strtok_r_token, "exit") == 0)
			{
				exit_flag = true;
				break;
			}

			// output token and token number
			fprintf(fp_out, "T%d: %s\n", token_number, strtok_r_token);
			token_number++;

			// continue tokenization - move pointers to next token
			strtok_r_token = strtok_r(NULL, " ", &strtok_r_ptr);
		}
    }
	/* leave a newline at the end of the program */
	fprintf(fp_out, "\n");

	/*Free the allocated memory*/
	free(command_string);

	/* close files */
	fclose(fp_in);
	fclose(fp_out);

	/*exit from program */
	return EXIT_SUCCESS;
}
/*-----------------------------Program End-----------------------------------*/