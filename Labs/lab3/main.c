/*
* Description: System Calls in C
*
* Author: Michael O'Connell
*
* Date: 10/15/20
*
* The goal of this program is to use system calls to implement a novel command 
* lfcat, which lists all files and their contents.
*
* This program generally works by:
* 1. Verifying user has called correct method (in main.c)
* 2. Obtaining the current working directory
* 3. Opening the current working directory
* 4. Read entries in the directory stream
* 5. Open each entry, get information (size, mode) about it, determine 
*	 if it is a file, and output it's contents to output.txt
* 6. Close entry (file)
*/
/*-------------------------Preprocessor Directives---------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "command.h"

/*---------------------------------------------------------------------------*/

/*-----------------------------Program Main----------------------------------*/
int main(int argc, char** argv) {
	
	/* string handling variables */
	size_t bufsize = 0;
	char* command_string = NULL;
	char* strtok_r_ptr;
	char* strtok_r_token;

	/* program/system variables */
	bool exit_flag = false;

	/* File handling variables */
	FILE* fp_out = fopen("output.txt", "w");
	
	/* Get commands from user */
	while(exit_flag == false) 
	{
		// get command from user
		fprintf(stdout, ">>> ");
		getline(&command_string, &bufsize, stdin);

		// remove newline character from user command
		command_string[strlen(command_string) - 1] = '\0';
		
		/* tokenize command string */
		strtok_r_token = strtok_r(command_string, " ", &strtok_r_ptr);
		while(strtok_r_token != NULL)
		{			
			// check if exit command has been called
			if(strcmp(strtok_r_token, "exit") == 0)
			{
				exit_flag = true;
				break;
			}
			// check if lfcat command has been called
			else if(strcmp(strtok_r_token, "lfcat") == 0)
			{
				lfcat();
				exit_flag = true;
			}
			// other commands not recognized, move on
			else
			{
				fprintf(stdout, "Error: Unrecognized command!\n");
			}

			// continue tokenization - move pointers to next token
			strtok_r_token = strtok_r(NULL, " ", &strtok_r_ptr);
		}
    }
	/* leave a newline at the end of the program */
	fprintf(stdout, "\n");

	/* Free the allocated memory */
	free(command_string);

	/* Close files */
	fclose(fp_out);

	/*exit from program */
	return EXIT_SUCCESS;
}
/*-----------------------------Program End-----------------------------------*/