#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include "command.h"

void lfcat()
{
    /* Directory handling variables */
    size_t size = 128;
    char* cwd_buf = (char*) malloc(sizeof(char) * size);
    DIR* cwd_ptr; // (current working) directory stream 
    struct dirent* directory_entry; // dirent structure representing directory entry in directory stream
    
    /* File handling variables */
    FILE* fp_out; // redirecting stdout to output.txt
    int fp_directory_entry; // for opening file in current directory entry
    struct stat file_stat; // for obtaining file size
    char* file_buf = (char*) malloc(sizeof(char) * size);
    
    /* Redirect STDOUT to output.txt */
    fp_out = freopen("output.txt", "w", stdout);

    /* Get current working directory */
    getcwd(cwd_buf, size);
    if(cwd_buf == NULL)
    {
        write(2, "Error getting current directory.\n", strlen("Error getting current directory.\n"));
        free(cwd_buf);
        exit(EXIT_FAILURE);
    }

    /* Open current working directory */
    cwd_ptr = opendir(cwd_buf);
    if(cwd_ptr == NULL)
    {
        write(2, "Error opening current directory.\n", strlen("Error opening current directory.\n"));
    }

    /* Read directory stream (all files in directory) */
    directory_entry = readdir(cwd_ptr);
    while(directory_entry != NULL)
    {
        if(strcmp(directory_entry->d_name, "output.txt") == 0)
        {
            directory_entry = readdir(cwd_ptr);
            continue;
        }

        /* Open file for reading */
        fp_directory_entry = open(directory_entry->d_name, O_RDONLY);
        if(!fp_directory_entry)
        {
            write(2, "Error opening file in directory stream.\n", strlen("Error opening file in directory stream.\n"));
        }

        /* Get info for file (we're using size and "mode" - whether file or directory) */
        if(stat(directory_entry->d_name, &file_stat) < 0)
        {
            write(2, "Error: failed to get file data.\n", strlen("Error: failed to get file data.\n"));
            write(2, "\n", 1);
        }

        /* If "file" (current position in directory stream) is a directory, skip it */
        if(S_ISDIR(file_stat.st_mode))
        {
            directory_entry = readdir(cwd_ptr);
            continue;
        }

        /* Write file names (from directory stream) to stdout (-1 as a return for write indicates error) */
        write(1, "FILE: ", strlen("FILE: "));
        if(write(1, directory_entry->d_name, strlen(directory_entry->d_name)) == -1)
        {
            write(2, "Error writing to standard out.\n", size);
        }

        /* Newline between file name and file content */
        write(1, "\n", 1);

        /* Size buffer to be the same size as file */
        file_buf = (char*) realloc(file_buf, sizeof(char) * file_stat.st_size);

        /* Read contents of file to buffer */
        read(fp_directory_entry, file_buf, file_stat.st_size);

        /* Read contents of file (buffer) to stdout */
        write(1, file_buf, file_stat.st_size);

        /* "Clear" contents of buffer */
        for(int i = 0; i < file_stat.st_size; i++)
        {
            file_buf[i] = '\0';
        }

        /* Close current file to get ready for next file */
        close(fp_directory_entry);

        directory_entry = readdir(cwd_ptr);

        /* Write 80 '-' characters between files */
        for(int i = 0; i < 80; i++)
        {
            write(1, "-", 1);
        }

        /* Newline in between file names */
        write(1, "\n", 1);
    }

    /* Deallocate memory */
    free(cwd_buf);
    free(file_buf);

    /* Close files */
    fclose(fp_out);

    /* Close directory */
    closedir(cwd_ptr);
}