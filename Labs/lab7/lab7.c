/*=============================================================================
 * Program Name: lab7 - Queues of thread-safe meal tickets
 * Class: CIS 415 - Operating Systems at University of Oregon, Fall 2020
 * Author: Michael O'Connell
 * Creator of skeleton: Jared Hall, Grayson Guan
 * Date: 11/29/2020
 * Description:
 *     An array of meal ticket queues (MTQ) is held in "registry". Each queue in
 *     the array is for a specific meal (e.g. "lunch", "dinner", etc.) and is implemented 
 *     as a circular ring buffer. The program creates threads that "push" onto
 *     a specified queue and threads that "pop" from a specified queue (these are
 *     publisher() and subscriber(), respectively). These threads are halted before
 *     they enqueue/push and dequeue/pop (via pdthread_cond_wait()) and then resumed
 *     after all the threads are created (via pthread_cond_broadcast() in main()). This
 *     is done so all the threads can start their operations at the same time. Each thread
 *     then enqueues/dequeues "MAXTICKETS" (3 for this specific program) number of times.
 *     Each queue in the registry array contains a mutex which allows for exclusive access
 *     to the queue, preventing concurrent enqueue/dequeue operations on that queue which 
 *     maintains consistency within the queue. The program ends when the publisher and subscriber
 *     threads finish enqueuing and dequeuing. Ensuring all threads finish to completion 
 *     is implemented via pthread_join() in main().  
 *===========================================================================*/

//========================== Preprocessor Directives ==========================
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
//=============================================================================

//================================= Constants =================================
#define MAXNAME 15
#define MAXQUEUES 4
#define MAXTICKETS 3
#define MAXDISH 20
#define MAXPUBs 4
#define MAXSUBS 4
//=============================================================================

//============================ Structs and Macros =============================
typedef struct mealTicket
{
	int ticketNum;
	char *dish;
} mealTicket;

typedef struct MTQ 
{
	char name[MAXNAME];
	struct mealTicket *buffer;
	int head;
	int tail;
	int max_length;
	int length;
	int ticket;
	pthread_mutex_t lock;
} MTQ;

typedef struct publisher_arguments
{
	mealTicket* tickets;
	char** MTQ_ID;
	int thread_id;
} publisher_arguments;

typedef struct subscriber_arguments
{
	char* MTQ_ID;
	int thread_id;
	mealTicket ticket;
	
} subscriber_arguments;

MTQ registry[MAXQUEUES];
pthread_mutex_t main_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t main_cond = PTHREAD_COND_INITIALIZER;

//=============================================================================

//================================= Functions =================================

/* Initalizes MTQ in registry at index "pos". 
   MTQ_ID is used to name the MTQ.
   Also note that mutex for MTQ is also initialized here. 
*/
void init(int pos, char *MTQ_ID) 
{
	strcpy(registry[pos].name, MTQ_ID);
	registry[pos].buffer = (struct mealTicket*) malloc(sizeof(struct mealTicket) * MAXTICKETS);
	registry[pos].head = 0;
	registry[pos].tail = 0;
	registry[pos].max_length = MAXTICKETS;
	registry[pos].length = 0;
	registry[pos].ticket = 0;
	pthread_mutex_init(&registry[pos].lock, NULL);
}

/* Helper method to free dynamically-allocated members of MTQ in the registry at 
   index "pos".
   Specifically, deallocates the "buffer" member holding the mealTickets.
*/
void freeMTQ(int pos, char *MTQ_ID) 
{
	free(registry[pos].buffer);
}

/* Helper method to allocate publisher_arguments for usage in the publisher() method.
   See the publisher_arguments struct and the publisher() method for the members that 
   are dynamically-allocated.
*/
void allocate_publisher_args(publisher_arguments** args_addr)
{
	publisher_arguments* publisher_args = *args_addr;
	for(int i = 0; i < MAXPUBs; i++)
	{
		publisher_args[i].tickets = (mealTicket*) malloc(sizeof(mealTicket) * MAXTICKETS);
		publisher_args[i].MTQ_ID = (char**) malloc(sizeof(char*) * MAXTICKETS);
		for(int j = 0; j <  MAXTICKETS; j++)
		{
			publisher_args[i].tickets[j].dish = (char*) malloc(sizeof(char) * MAXNAME);
			publisher_args[i].MTQ_ID[j] = (char*) malloc(sizeof(char) * MAXNAME);
		}
	}
}

/* Helper method to allocate subscriber_arguments for usage in the subscriber() method.
   See the subscriber_arguments struct and the subscriber() method for the members that 
   are dynamically-allocated.
*/
void allocate_subscriber_args(subscriber_arguments** args_addr)
{
	subscriber_arguments* subscriber_args = *args_addr;
	for(int i = 0; i < MAXSUBS; i++)
	{
		subscriber_args[i].MTQ_ID = (char*) malloc(sizeof(char) * MAXNAME);
		subscriber_args[i].ticket.dish = (char*) malloc(sizeof(char) * MAXNAME);
	}
}

/* Helper method to deallocate publisher_arguments for usage in the publisher() method.
   See the publisher_arguments struct and the publisher() method for the members that 
   are dynamically-allocated.
*/
void deallocate_publisher_args(publisher_arguments** args_addr)
{
	publisher_arguments* publisher_args = *args_addr;
	for(int i = 0; i < MAXPUBs; i++)
	{
		for(int j = 0; j < MAXTICKETS; j++)
		{
			free(publisher_args[i].tickets[j].dish);
			free(publisher_args[i].MTQ_ID[j]);
		}

		free(publisher_args[i].tickets);
		free(publisher_args[i].MTQ_ID);
	}
}

/* Helper method to deallocate subscriber_arguments for usage in the subscriber() method.
   See the subscriber_arguments struct and the subscriber() method for the members that 
   are dynamically-allocated.
*/
void deallocate_subscriber_args(subscriber_arguments** args_addr)
{
	subscriber_arguments* subscriber_args = *args_addr;
	for(int i = 0; i < MAXSUBS; i++)
	{
		free(subscriber_args[i].MTQ_ID);
		free(subscriber_args[i].ticket.dish);
	}
}

/* Helper method that takes a MTQ_ID and compares it against each registry array's 
   "names" member. 
   Returns the registry index with the "names" member that matches MTQ_ID.
*/
int get_registry_index(char* MTQ_ID)
{
    for(int i = 0; i < MAXQUEUES; i++)
    {
        if(strcmp(MTQ_ID, registry[i].name) == 0)
        {
            return i;
        }
        // Didn't find desired MTQ
        else if(i == MAXQUEUES - 1)
        {
            return -1;
        }
    }
}

/* enqueue() attempts to push a mealTicket into the registry's (specified by MTQ_ID) "buffer"
   member. The method uses a the registry's mutex (the registry's "lock" member) for exclusive 
   access to the buffer. 
   
   If the index is not found an error is outputted and -1 is returned. If the registry's
   buffer is full a message is outputted and 0 is returned. If the operation is successful,
   a message is outputted and 0 is returned.

   The queue (registry's "buffer" member) is a circular ring buffer. Hence head and tail will be "incremented"
   to 0 after they've reached registry's max_length - 1.

   Implementation:
   1. Finds the registry index.
   2. Aquire mutex lock.
   3. Perform Enqueue operation:
      1. Check if registry's buffer is full.
	  2. If not, push MT onto the "head" (registry member) of the registry buffer.
	  3. Increment the head pointer.
	4. Release mutex lock. 
*/
int enqueue(char *MTQ_ID, mealTicket *MT) 
{
	//Step-1: Find registry (index)
    int target_index = get_registry_index(MTQ_ID);
	if(target_index == -1)
	{
		printf("Enqueue Error: Didn't find correct registry based on MTQ_ID. Check MTQ_ID.\n");
		printf("MTQ_ID is: %s\n", MTQ_ID);
		return -1;
	}

	//STEP-2: 
	//Aquire the lock if it's available. Otherwise, wait until it is.
	pthread_mutex_lock(&registry[target_index].lock);

	//SETP-3: enqueue

	/* Check whether registry's buffer is full */
    if(registry[target_index].length == registry[target_index].max_length)
    {
        printf("Publisher is pushing: Queue: %s - Error Queue is full. Thread ID: %ld\n", 
		        registry[target_index].name, pthread_self());
		pthread_mutex_unlock(&registry[target_index].lock);
		return 0;
    }

    /* Push meal ticket onto MTQ_target queue */
    registry[target_index].buffer[registry[target_index].head] = *MT;
    registry[target_index].length++;

    /* Increment head pointer so it always points to the 
       position after the newest element in the queue 
    */
    if(registry[target_index].head < registry[target_index].max_length - 1)
    {
        registry[target_index].head++;
    }
    else
    {
        registry[target_index].head = 0;
    }

    printf("pushing: Queue: %s - Ticket Number: %d - Dish: %s - Thread ID: %ld\n", 
	        registry[target_index].name, MT->ticketNum, MT->dish, pthread_self());

	//Release the lock.
	pthread_mutex_unlock(&registry[target_index].lock);
}

/* dequeue() attempts to pop a mealTicket from the registry's (specified by MTQ_ID) "buffer"
   member. The method fills an empty mealTicket ("MT") with the information from the registry's buffer
   at the tail location in order to output the ticket information after a successful operation. 
   The method uses a the registry's mutex (the registry's "lock" member) for exclusive access to the buffer.

   The queue (registry's "buffer" member) is a circular ring buffer. Hence head and tail will be "incremented"
   to 0 after they've reached registry's max_length - 1.
   
   If the index is not found an error is outputted and -1 is returned. If the registry's
   buffer is empty a message is outputted and 0 is returned. If the operation is successful,
   a message is outputted and 0 is returned.

   Implementation:
   1. Finds the registry index.
   2. Aquire mutex lock.
   3. Perform Dequeue operation:
      1. Check if registry's buffer is full.
	  2. If not, copy the mealTicket information from the registry buffer.
	  3. Increment the tail pointer.
   4. Release mutex lock. 
*/
int dequeue(char *MTQ_ID, int ticketNum, mealTicket *MT) 
{
	//Step-1: Find registry
    int target_index = get_registry_index(MTQ_ID);
	if(target_index == -1)
	{
		printf("Dequeue Error: Didn't find correct registry based on MTQ_ID. Check MTQ_ID.\n");
		printf("MTQ_ID is: %s\n", MTQ_ID);
		return -1;
	}


	//Step-2:
	//Aquire the lock if it's available. Otherwise, wait until it is.
	pthread_mutex_lock(&registry[target_index].lock);

	//Setp-3: dequeue

    /* Check if MTQ_target is empty */
    if(registry[target_index].length == 0)
    {
        printf("popping: queue: %s. Queue is empty, nothing to pop. Thread ID: %ld\n", 
		       registry[target_index].name, pthread_self());
		pthread_mutex_unlock(&registry[target_index].lock);
		return 0;
    }

    /* Get info to fill empty meal ticket */
    strcpy(MT->dish, registry[target_index].buffer[registry[target_index].tail].dish);
    MT->ticketNum = registry[target_index].buffer[registry[target_index].tail].ticketNum;

    /* Pop from MTQ_target - 
       Increment tail pointer so it always points to the 
       position after the newest element in the queue 
    */
    if(registry[target_index].tail < registry[target_index].max_length - 1)
    {
        registry[target_index].tail++;
    }
    else
    {
        registry[target_index].tail = 0;
    }

    registry[target_index].length--;
	
	printf("popping: queue: %s, ticketNum: %d, dish: %s, thread ID: %ld\n", 
	        registry[target_index].name, MT->ticketNum, MT->dish, pthread_self());

	// release the lock
	pthread_mutex_unlock(&registry[target_index].lock);

	return 0;	
}

/* The publisher will receive the following in the struct args:
*   1. An array of mealTickets to push to the queue.
*   2. For each meal ticket, an MTQ_ID. (Note: a publisher can push to multiple queues)
*   3. The thread ID
*
* The publisher will do the pthread_cond_wait procedure, and wait for a cond signal to begin its work
* The publisher will then print its type and thread ID on startup. Then it will push one meal ticket at a time to
* its appropriate queue before sleeping for 1 second. It will do this until there are no more meal tickets to push.
*/
void *publisher(void *args) 
{
	// Cast args to the correct type in order to use throughout function
	publisher_arguments* args_cpy = (publisher_arguments*) args;

	// Have all threads wait so they can start at the same time (after recieving signal from main() via broadcast)
	printf("type: publisher, thread id: %ld is waiting to start\n", pthread_self());
	pthread_mutex_lock(&main_mutex);
	pthread_cond_wait(&main_cond, &main_mutex);
	pthread_mutex_unlock(&main_mutex);
	printf("type: publisher, thread id: %ld is starting\n", pthread_self());
	
	// push meal tickets until there are none left
	for(int i = 0; i < MAXTICKETS; i++)
	{
		enqueue(args_cpy->MTQ_ID[i], &args_cpy->tickets[i]);
		// allow other threads to do work while this thread sleeps
		sched_yield();
		sleep(1);
	}
}

/* The subscriber will take the following:
*       1. The MTQ_ID it will pull from.
*       2. The thread ID
*       3. An empty meal ticket.
*
* The subscriber will do the pthread_cond_wait procedure, and wait for a cond signal to begin its work
* The subscriber will print its type and thread ID on startup. Then it will pull a ticket from its queue
* and print it. If the queue is empty then it will print an empty message along with its
* thread ID and wait for 1 second. If the thread is not empty then it will pop a ticket and 
* print it along with the thread id (Note: this occurs in dequeue()).
*/
void *subscriber(void *args) 
{
	// Cast args to the correct type in order to use throughout function
	subscriber_arguments* args_cpy = (subscriber_arguments*) args;

	// Have all threads wait so they can start at the same time (after recieving signal from main() via broadcast)
	printf("type: subscriber, thread id: %ld is waiting to start\n", pthread_self());
	pthread_mutex_lock(&main_mutex);
	pthread_cond_wait(&main_cond, &main_mutex);
	pthread_mutex_unlock(&main_mutex);
	printf("type: subscriber, thread id: %ld is starting up\n", pthread_self());

	for(int i = 0; i < MAXTICKETS; i++)
	{
		dequeue(args_cpy->MTQ_ID, i, &args_cpy->ticket);
		// allow other threads to do work while this thread sleeps
		sched_yield();
		sleep(1);
	}

}
//=============================================================================

//=============================== Program Main ================================
int main(int argc, char argv[]) {
	//Variables Declarations
	char *qNames[] = {"Breakfast", "Lunch", "Dinner", "Bar"};
	char *bFood[] = {"Eggs", "Bacon", "Steak"};
	char *lFood[] = {"Burger", "Fries", "Coke"};
	char *dFood[] = {"Steak", "Greens", "Pie"};
	char *brFood[] = {"Whiskey", "Sake", "Wine"};
	int i, j, t = 1;
	int test[4];
	char dsh[] = "Empty";
	mealTicket bfast[3] = {[0].dish = bFood[0], [1].dish = bFood[1], [2].dish = bFood[2]};
	mealTicket lnch[3] = {[0].dish = lFood[0], [1].dish = lFood[1], [2].dish = lFood[2]};
	mealTicket dnr[3] = {[0].dish = dFood[0], [1].dish = dFood[1], [2].dish = dFood[2]};
	mealTicket br[3] = {[0].dish = brFood[0], [1].dish = brFood[1], [2].dish = brFood[2]};
	mealTicket* allFood[MAXQUEUES] = {bfast, lnch, dnr, br}; 
	mealTicket ticket = {.ticketNum=0, .dish=dsh};
	
	//STEP-1: Initialize the registry
    for(int i = 0; i < MAXQUEUES; i++)
    {
        init(i, qNames[i]);
    }
	
	//STEP-2: Create the publisher thread-pool
	pthread_t publisher_arr[MAXPUBs];

	// allocate arguments for each thread
	publisher_arguments* publisher_args = (publisher_arguments*) malloc(sizeof(publisher_arguments) * MAXPUBs);
	allocate_publisher_args(&publisher_args);

	// initalize arguments for each thread
	for(int i = 0; i < MAXPUBs; i++)
	{
		for(int j = 0; j < MAXTICKETS; j++)
		{
			strcpy(publisher_args[i].MTQ_ID[j], qNames[i]);
			strcpy(publisher_args[i].tickets[j].dish, allFood[i][j].dish);
			publisher_args[i].tickets[j].ticketNum = j;
		}
		
		publisher_args[i].thread_id = 0;
		
		// create the threads
		pthread_create(&publisher_arr[i], NULL, publisher, &publisher_args[i]);
	}

	//STEP-3: Create the subscriber thread-pool
	pthread_t subscriber_arr[MAXSUBS];
	subscriber_arguments* subscriber_args = (subscriber_arguments*) malloc(sizeof(subscriber_arguments) * MAXSUBS);
	allocate_subscriber_args(&subscriber_args);

	// initialize arguments for each thread
	for(int i = 0; i < MAXSUBS; i++)
	{
		strcpy(subscriber_args[i].MTQ_ID, qNames[i]);
		subscriber_args[i].thread_id = 0;
		subscriber_args[i].ticket.ticketNum = 0;

		// create the threads
		pthread_create(&subscriber_arr[i], NULL, subscriber, &subscriber_args[i]);
	}

	// Start all threads together
	printf("Waiting for 5 seconds to ensure all threads are created.\n");
	sleep(5);
	printf("Sending signal (via pthread_cond_broadcast()) to all threads\n");
	pthread_mutex_lock(&main_mutex);
	pthread_cond_broadcast(&main_cond);
	pthread_mutex_unlock(&main_mutex);
	
	//STEP-4: Join the thread-pools
	for(int i = 0; i < MAXPUBs; i++)
	{
		pthread_join(publisher_arr[i], NULL);
	}

	for(int i = 0; i < MAXSUBS; i++)
	{
		pthread_join(subscriber_arr[i], NULL);
	}		
	
	// deallocate arguments for each thread
	deallocate_publisher_args(&publisher_args);
	deallocate_subscriber_args(&subscriber_args);
	free(publisher_args);
	free(subscriber_args);

	// STEP-5: Free the registry 
	for(int i = 0; i < MAXQUEUES; i++)
	{
		freeMTQ(i, qNames[i]);
	}

		
	return EXIT_SUCCESS;
}
//=============================================================================